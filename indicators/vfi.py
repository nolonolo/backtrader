#!/usr/bin/env python
# -*- coding: utf-8; py-indent-offset:4 -*-
###############################################################################
#
# Copyright (C) 2015, 2016, 2017 Daniel Rodriguez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

from . import Indicator, MovAv, StdDev
import backtrader as bt
from backtrader.indicators import SumN

class Vfi(Indicator):
    '''
    See:
      - http://www.vortexindicator.com/VFX_VORTEX.PDF

    '''
    lines = ('vfi',)

    params = (('length', 130),('_movav2', MovAv.Simple),('dev_period', 30),
              ('coef', 0.2),('vcoef', 2.5))

    plotlines = dict(vfi=dict(color='red'))

    def __init__(self):
        typical = (self.data.close(0) + self.data.high(0) + self.data.low(0))/3
        inter = self.data.openinterest(0)
        vol = self.data.volume(0)
        vinter = StdDev(inter, movav=self.p._movav2, period=self.p.dev_period)
        cutoff = self.p.coef * vinter * self.data.close(0)
        vave = self.p._movav2(vol, period=self.params.length)(-1)

        vmax = vave * self.p.vcoef
        vc = bt.If(vol < vmax, vol, vmax)
        mf = typical - typical(-1)
        vcp = bt.If(mf > cutoff, vc, bt.If(mf < -cutoff, -vc, 0))

        self.lines.vfi = SumN(vcp,period = 130)/vave


#!/usr/bin/env python
# -*- coding: utf-8; py-indent-offset:4 -*-
###############################################################################
#
# Copyright (C) 2015, 2016, 2017 Daniel Rodriguez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

from . import MovingAverageBase, ExponentialSmoothing, PeriodN


class ExponentialMovingAverage(MovingAverageBase):
    '''
    A Moving Average that smoothes data exponentially over time.

    It is a subclass of SmoothingMovingAverage.

      - self.smfactor -> 2 / (1 + period)
      - self.smfactor1 -> `1 - self.smfactor`

    Formula:
      - movav = prev * (1.0 - smoothfactor) + newdata * smoothfactor

    See also:
      - http://en.wikipedia.org/wiki/Moving_average#Exponential_moving_average
    '''
    alias = ('EMA', 'MovingAverageExponential',)
    lines = ('ema',)
    params = (('talpha', None),)

    def __init__(self):
        self.talpha = self.p.talpha
        if self.talpha is None:
            self.talpha = (2.0 / (1 + self.p.period))  # def EMA value

        # Before super to ensure mixins (right-hand side in subclassing)
        # can see the assignment operation and operate on the line
        self.lines[0] = es = ExponentialSmoothing(
            self.data,
            period=self.p.period,
            alpha = self.p.talpha)

        self.alpha, self.alpha1 = es.alpha, es.alpha1

        super(ExponentialMovingAverage, self).__init__()

class TV_EMA(PeriodN):
    
    alias = ('TV_EMA',)    
    params = {('period', 30),}  # even if defined, we can redefine the default value
    lines = ('ema',)  # our output line

    def __init__(self):
        self.alpha = 2.0 / (1.0 + self.p.period)  # period -> exp smoothing factor

        
    def nextstart(self):  # calculate here the seed value
        self.lines.ema[0] = self.data.get(size=self.p.period) / self.p.period

    def next(self):
        ema1 = self.lines.ema[-1]  # previous EMA value
        self.lines.ema[0] = ema1 * (1.0 - self.alpha) + self.data[0] * self.alpha

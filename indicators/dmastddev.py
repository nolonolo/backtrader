#!/usr/bin/env python
# -*- coding: utf-8; py-indent-offset:4 -*-
###############################################################################
#
# Copyright (C) 2015, 2016, 2017 Daniel Rodriguez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)


from . import MovingAverageBase, MovAv, ZeroLagIndicator, StdDev, TrueRange


class Dmastddev(MovingAverageBase):
    '''By Nathan Dickson

    The *Dickson Moving Average* combines the ``ZeroLagIndicator`` (aka
    *ErrorCorrecting* or *EC*) by *Ehlers*, and the ``HullMovingAverage`` to
    try to deliver a result close to that of the *Jurik* Moving Averages

    Formula:
      - ec = ZeroLagIndicator(period, gainlimit)
      - hma = HullMovingAverage(hperiod)

      - dma = (ec + hma) / 2

      - The default moving average for the *ZeroLagIndicator* is EMA, but can
        be changed with the parameter ``_movav``

        .. note:: the passed moving average must calculate alpha (and 1 -
                  alpha) and make them available as attributes ``alpha`` and
                  ``alpha1``

      - The 2nd moving averag can be changed from *Hull* to anything else with
        the param *_hma*

    See also:
      - https://www.reddit.com/r/algotrading/comments/4xj3vh/dickson_moving_average
    '''
    alias = ('DMADEV', 'DicksonMADev',)
    lines = ('dma_mid','t_dev','b_dev')
    params = (
        ('gainlimit', 50),
        ('period', 40),
        ('hperiod', 40),
        ('_movav', MovAv.EMA),
        ('_hma', MovAv.HMA),
        ('_movav2', MovAv.Simple),
        ('dev_period', 40),
        ('devfactor', 2)
    )
    plotlines = dict(dma_mid=dict(color='yellow'),
                     t_dev=dict(color='red'),
                     b_dev=dict(color='green'),)


    def _plotlabel(self):
        plabels = [self.p.period, self.p.gainlimit, self.p.hperiod]
        plabels += [self.p._movav] * self.p.notdefault('_movav')
        plabels += [self.p._hma] * self.p.notdefault('_hma')
        return plabels

    def __init__(self):
        
        ema1 = self.p._movav(self.data.close, period=self.p.period )
        ema2 = self.p._movav(ema1, period=self.p.period)
        diff = ema1 - ema2
        ec = ema1 + diff

        hull = self.p._hma(period=self.p.hperiod)

        self.lines.dma = dm = (ec + hull) / 2.0

        stddev = self.p.devfactor * StdDev(self.data, period=self.p.dev_period,
                                           movav=self.p._movav2)
        self.lines.t_dev = dm + stddev
        self.lines.b_dev = dm - stddev
        
        # To make mixins work - super at the end for cooperative inheritance
        super(Dmastddev, self).__init__()

#############################################################################

class Dmastddev_adp(MovingAverageBase):
    '''By Nathan Dickson

    The *Dickson Moving Average* combines the ``ZeroLagIndicator`` (aka
    *ErrorCorrecting* or *EC*) by *Ehlers*, and the ``HullMovingAverage`` to
    try to deliver a result close to that of the *Jurik* Moving Averages

    Formula:
      - ec = ZeroLagIndicator(period, gainlimit)
      - hma = HullMovingAverage(hperiod)

      - dma = (ec + hma) / 2

      - The default moving average for the *ZeroLagIndicator* is EMA, but can
        be changed with the parameter ``_movav``

        .. note:: the passed moving average must calculate alpha (and 1 -
                  alpha) and make them available as attributes ``alpha`` and
                  ``alpha1``

      - The 2nd moving averag can be changed from *Hull* to anything else with
        the param *_hma*

    See also:
      - https://www.reddit.com/r/algotrading/comments/4xj3vh/dickson_moving_average
    '''
    alias = ('DMADEV_adp',)
    lines = ('dma_mid','t_dev','b_dev','ltr',)
    params = (
        ('gainlimit', 50),
        ('period', 40),
        ('hperiod', 40),
        ('_movav', MovAv.EMA),
        ('_hma', MovAv.HMA),
        ('_movav2', MovAv.Simple),
        ('dev_period', 40),
        ('devfactor', 2),
        ('atr_len', 14),
    )
    plotlines = dict(dma_mid=dict(color='yellow'),
                     t_dev=dict(color='red'),
                     b_dev=dict(color='green'),)


    def _plotlabel(self):
        plabels = [self.p.period, self.p.gainlimit, self.p.hperiod]
        plabels += [self.p._movav] * self.p.notdefault('_movav')
        plabels += [self.p._hma] * self.p.notdefault('_hma')
        return plabels

    def __init__(self):
        ec = ZeroLagIndicator(period=self.p.period,
                              gainlimit=self.p.gainlimit,
                              _movav=self.p._movav)

        hull = self.p._hma(period=self.p.hperiod)

        atr  = self.p._movav2(TrueRange(self.data), period=self.p.atr_len)
     
        atr2 = (((atr * 13) + TrueRange(self.data)) / 14)
        
        self.lines.ltr = atr2 / (self.data.close * 100)

        self.lines.dma_mid = self.dm =(ec + hull) / 2.0
 
        stddev = (self.p.devfactor + self.lines.ltr(0)) * StdDev(self.data, period=self.p.dev_period,
                                           movav=self.p._movav2)
        
        self.lines.t_dev = self.dm + stddev
        self.lines.b_dev = self.dm - stddev
        
        # To make mixins work - super at the end for cooperative inheritance
        super(Dmastddev_adp, self).__init__()       




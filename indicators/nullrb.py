# -*- coding: utf-8 -*-
"""
Created on Mon Jan 29 11:03:40 2018

@author: ADMIN
"""
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)


from . import MovingAverageBase, MovAv,HMAEnvelope,HMA

import backtrader as bt
import backtrader.indicators as btind


class nullrb(bt.indicators.HMAEnvelope):
    
    lines = ('null','top1','bot1',)
    params = (('period', 250),('perc', 6))
    
    plotlines = dict(
        null=dict(_name='HMA colored', color= 'green' ),
        top1=dict(color='yellow'),
        bot1=dict(color='yellow')    
    )   

    def __init__(self):
        
        self.lines.null[0] = btind.hma(self.data, period=self.p.period)
        self.lines.top1[0] = self.lines.hull[0] * self.params.perc
        self.lines.bot1[0] = self.lines.hull[0] * self.params.perc
   
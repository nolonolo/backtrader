#!/usr/bin/env python
# -*- coding: utf-8; py-indent-offset:4 -*-
###############################################################################
#
# Copyright (C) 2015, 2016, 2017 Daniel Rodriguez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)


from . import MovingAverageBase, MovAv, StdDev


# Inherits from MovingAverageBase to auto-register as MovingAverage type
class Hullstddev(MovingAverageBase):
    '''By Alan Hull

    The Hull Moving Average solves the age old dilemma of making a moving
    average more responsive to current price activity whilst maintaining curve
    smoothness. In fact the HMA almost eliminates lag altogether and manages to
    improve smoothing at the same time.

    Formula:
      - hma = wma(2 * wma(data, period // 2) - wma(data, period), sqrt(period))

    See also:
      - http://alanhull.com/hull-moving-average

    Note:

      - Please note that the final minimum period is not the period passed with
        the parameter ``period``. A final moving average on moving average is
        done in which the period is the *square root* of the original.

        In the default case of ``30`` the final minimum period before the
        moving average produces a non-NAN value is ``34``
    '''
    alias = ('HDEV')
    lines = ('hma_mid','t_dev','b_dev')


    # param 'period' is inherited from MovingAverageBase
    params = (('_movav', MovAv.WMA),('period', 250),('period1', 40), ('devfactor', 2.0))
    plotlines = dict(hma_mid=dict(color='yellow'),
                     t_dev=dict(color='red'),
                     b_dev=dict(color='green'),
    )
    
    def __init__(self):
        wma = self.p._movav(self.data, period=self.params.period)
        wma2 = 2.0 * self.p._movav(self.data, period=self.params.period // 2)

        sqrtperiod = pow(self.params.period, 0.5)
        self.lines.hma_mid = hm = self.p._movav(wma2 - wma, period=int(sqrtperiod))

        stddev = self.p.devfactor * StdDev(hm, period=self.p.period1,
                                           movav=self.p._movav)
        self.lines.t_dev = hm + stddev
        self.lines.b_dev = hm - stddev
        

        # Done after calc to ensure coop inheritance and composition work
        super(Hullstddev, self).__init__()

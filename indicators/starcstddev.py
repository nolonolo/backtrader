#!/usr/bin/env python
# -*- coding: utf-8; py-indent-offset:4 -*-
###############################################################################
#
# Copyright (C) 2015, 2016, 2017 Daniel Rodriguez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either vemfion 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

from . import Indicator, Max, MovAv, StdDev, Sum
from . import DivZeroByZero
from backtrader.indicators import SumN
import backtrader as bt

class STARCStdDev(Indicator):

    alias = ('STARC',)

    lines = ('starc','t_dev','b_dev')
    params = (
        ('STARC_len', 30),
        ('movav', MovAv.Simple),
        ('dev_period', 30),
        ('devfactor', 3)
    )
    plotlines = dict(starc=dict(color='yellow'),
                     t_dev=dict(color='red'),
                     b_dev=dict(color='green'),)
    
    def _plotlabel(self):
        plabels = [self.p.STARC_len]
        plabels += [self.p.movav] * self.p.notdefault('movav')
        return plabels

    def __init__(self):

        x1 =  ( self.data.high(0) - self.data.low(0) )
        x2 =  abs( self.data.high(0) - self.data.close(-1) )
        x3 =  abs( self.data.low(0) - self.data.close(-1) )
        tr_v = bt.Max(x1, bt.Max(x2,x3))

        atr = self.p.movav(tr_v, period=self.p.STARC_len)
        
        self.lines.starc = base = self.p.movav(self.data.close, period=self.p.STARC_len)

        stddev = self.p.devfactor * atr
        
        self.lines.t_dev = base + stddev
        self.lines.b_dev = base - stddev
        
        super(STARCStdDev, self).__init__()



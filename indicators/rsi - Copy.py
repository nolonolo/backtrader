#!/usr/bin/env python
# -*- coding: utf-8; py-indent-offset:4 -*-
###############################################################################
#
# Copyright (C) 2015, 2016, 2017 Daniel Rodriguez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

from . import Indicator, Max, MovAv, StdDev, Sum
from . import DivZeroByZero
import backtrader as bt

#class Win(Indicator):
#    '''
#    Defined by J. Welles Wilder, Jr. in 1978 in his book *"New Concepts in
#    Technical Trading Systems"* for the RSI
#
#    Recods days which have been "up", i.e.: the close price has been
#    higher than the day before.
#
#    Formula:
#      - win = max(close - close_prev, 0)
#
#    See:
#      - http://en.wikipedia.org/wiki/Relative_strength_index
#    '''
#    lines = ('win',)
#    params = (('period', 1),)
#
#    def __init__(self):
#        self.diff = self.data.close - self.data.close(-1)
#        super(Win, self).__init__()
#
#    def next(self):
#        tiff = 0.001
#        if self.diff > 0:
#            tiff = self.diff
#
#        self.lines.win = tiff
#
#
#class Lose(Indicator):
#    '''
#    Defined by J. Welles Wilder, Jr. in 1978 in his book *"New Concepts in
#    Technical Trading Systems"* for the RSI
#
#    Recods days which have been "down", i.e.: the close price has been
#    lower than the day before.
#
#    Formula:
#      - lose = max(close_prev - close, 0)
#
#    See:
#      - http://en.wikipedia.org/wiki/Relative_strength_index
#    '''
#    lines = ('lose',)
#    params = (('period', 1),)
#
#    def __init__(self):
#        self.diff = self.data.close - self.data.close(-1)
#        super(Lose, self).__init__()
#
#    def next(self):
#        tiff =0.001
#        if self.diff < 0:
#            tiff = self.diff
#        self.lines.lose = tiff


class RSIStdDev(Indicator):
    '''Defined by J. Welles Wilder, Jr. in 1978 in his book *"New Concepts in
    Technical Trading Systems"*.

    It measures momentum by calculating the ration of higher closes and
    lower closes after having been smoothed by an average, normalizing
    the result between 0 and 100

    Formula:
      - up = win(data)
      - down = lose(data)
      - maup = movingaverage(up, period)
      - madown = movingaverage(down, period)
      - rs = maup / madown
      - rsi = 100 - 100 / (1 + rs)

    The moving average used is the one originally defined by Wilder,
    the SmoothedMovingAverage

    See:
      - http://en.wikipedia.org/wiki/Relative_strength_index

    Notes:
      - ``safediv`` (default: False) If this parameter is True the division
        rs = maup / madown will be checked for the special cases in which a
        ``0 / 0`` or ``x / 0`` division will happen

      - ``safehigh`` (default: 100.0) will be used as RSI value for the
        ``x / 0`` case

      - ``safelow``  (default: 50.0) will be used as RSI value for the
        ``0 / 0`` case
    '''
    alias = ('RSIDEV',)

    lines = ('rsi','t_dev','b_dev','winp','losep')
    params = (
        ('period', 40),
        ('movav', MovAv.Smoothed),
        ('movav2', MovAv.Simple),
        ('safediv', False),
        ('safehigh', 100.0),
        ('safelow', 50.0),
        ('dev_period', 40),
        ('_movav1', MovAv.WMA),
        ('devfactor', 2.5)
    )
    plotlines = dict(rsi=dict(color='yellow'),
                     t_dev=dict(color='red'),
                     b_dev=dict(color='green'),)
    def _plotlabel(self):
        plabels = [self.p.period]
        plabels += [self.p.movav] * self.p.notdefault('movav')
        return plabels

    def __init__(self):
        self.diff = (self.data.close - self.data.close(-1))
        
        self.lines.winp = win = Sum(bt.If(self.diff > 0,self.diff,0),self.p.period)
        self.lines.losep = lose = Sum(bt.If(self.diff < 0,abs(self.diff),0),self.p.period)     

        avg_win = win/self.p.period
        avg_lose = lose/self.p.period

        maup = self.p.movav(avg_win, period=self.p.period)
        madown = self.p.movav(avg_lose, period=self.p.period)        

        rs = maup / madown


        self.lines.rsi = 100.0 - 100.0 / (1.0 + rs)

        base = self.p.movav2(self.lines.rsi, period=self.p.dev_period)

        stddev = self.p.devfactor * StdDev(self.lines.rsi, period=self.p.dev_period,
                                           movav=self.p._movav1)
        self.lines.t_dev = base + stddev
        self.lines.b_dev = base - stddev
        
        super(RSIStdDev, self).__init__()

    def _rscalc(self, rsi):
        try:
            rs = (-100.0 / (rsi - 100.0)) - 1.0
        except ZeroDivisionError:
            return float('inf')

        return rs


##class RSI_Safe(RSI):
##    '''
##    Subclass of RSI which changes parameers ``safediv`` to ``True`` as the
##    default value
##
##    See:
##      - http://en.wikipedia.org/wiki/Relative_strength_index
##    '''
##    params = (('safediv', True),)
##
##
##class RSI_SMA(RSI):
##    '''
##    Uses a SimpleMovingAverage as described in Wikipedia and other soures
##
##    See:
##      - http://en.wikipedia.org/wiki/Relative_strength_index
##    '''
##    alias = ('RSI_Cutler',)
##
##    params = (('movav', MovAv.Simple),)
##
##
##class RSI_EMA(RSI):
##    '''
##    Uses an ExponentialMovingAverage as described in Wikipedia
##
##    See:
##      - http://en.wikipedia.org/wiki/Relative_strength_index
##    '''
##    params = (('movav', MovAv.Exponential),)
##

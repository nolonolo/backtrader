import pandas as pd
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])
import numpy as np

lista = ['09',10,11,12,13,14,15,16,17]
df2 = pd.DataFrame()

for i in lista:
    
    modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
    datapath = os.path.join(modpath, 'H:\AlgoTrad\Backtesting\XAUUSD\DAT_ASCII_XAUUSD_M1_20' + str(i) + '.csv')


    df = pd.read_csv(datapath, parse_dates = ['timestamp'], dayfirst=False, na_values=[np.nan],
                 usecols=['timestamp', 'open','high','low','close'], index_col = 0)


    # Created a dictionary to tell Pandas how to re-sample, if this isn't in place it will re-sample each column separately 

    ohlc_dict = {'open':'first', 'high':'max', 'low':'min', 'close': 'last'}

    # Resample to 5Min (this format is needed) as per ohlc_dict, then remove any line with a NaN
    df = df.resample('5Min', how=ohlc_dict)
    #df.replace(r'\s+', np.nan, regex=True)
    # Resample mixes the columns so lets re-arrange them 
    cols=['open', 'high', 'low', 'close']  
    df = df[cols]
    
    # Take rows with finite OLHC vaule ( Avoids NaN values)
    df = df[np.isfinite(df['open'])]
    
    #df2 = df2.append(df)
    
    # Write out to CSV
    df.to_csv('data\XBatch\XAUUSD-' + str(i) + '-5M.csv')
    
#df2.to_csv('data\XAUUSD-9to11-5M.csv')

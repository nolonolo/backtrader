# -*- coding: utf-8; py-indent-offset:4 -*-
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)
import time
from datetime import datetime, timedelta
import backtrader as bt


class TestStrategy(bt.Strategy):
    def start(self):
        self.counter = 0
        print('START')

    def prenext(self):
        self.counter += 1
        print('prenext len %d - counter %d' % (len(self), self.counter))

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.dataclose = self.data.close
        self.datahigh = self.data.high
        self.datalow = self.data.low
        self.datavolume = self.data.volume

    def next(self):
        print('------ next len %d - counter %d' % (len(self), self.counter))

        self.counter += 1
            
        print('*' * 5, 'NEXT:', bt.num2date(self.data0.datetime[0]), 'Symbol: ',self.data0._name, 'Open: ', self.data0.open[0], 'High: ', self.data0.high[0],
              'Low: ', self.data0.low[0], 'Close: ', self.data0.close[0], 'Volume: ', self.data0.volume[0],
              bt.TimeFrame.getname(self.data0._timeframe), len(self.data0))

if __name__ == '__main__':
    cerebro = bt.Cerebro()
    
    hist_start_date = datetime.utcnow() - timedelta(minutes=60)
    data_ticks = bt.feeds.CCXT(exchange='bitfinex', symbol='EOS/BTC', name="eos_btc_tick",
                           timeframe=bt.TimeFrame.Minutes, fromdate=hist_start_date, compression=5)

    cerebro.adddata(data_ticks, name="eos_btc_min")

    cerebro.addstrategy(TestStrategy)
    cerebro.run()
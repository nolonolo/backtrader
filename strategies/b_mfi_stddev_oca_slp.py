from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import datetime  # For datetime objects
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])
import warnings
warnings.filterwarnings("ignore")

# Import the backtrader platform
import backtrader as bt
import backtrader.feeds as btfeeds
from backtrader.indicators import mfistddev
# Create a Stratey
class BTCUSD_5(bt.Strategy):
    
    def log(self, txt, dt=None):
        #''' Logging function for this strategy'''
        dt = dt or self.datas[0].datetime.datetime(0)
#        print('%s' % (txt))

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.dataclose = self.datas[0].close
        

        # To keep track of pending orders and buy price/commission
        self.order = None
        self.buyprice = None
        self.buycomm = None
        
        self.ma1 = 1500
        self.ma2 = 500
        self.dev = 4
        # Indicators
#        self.stoch = bt.indicators.StochasticFull(self.datas[0], period=14, period_dslow = 4, safediv=True)
#        self.hull1 = bt.indicators.HullMovingAverage(self.datas[1], period=250)
#        self.hull_d = kamastddev.Kamastddev(self.datas[0], period=250, dev_period = 250, devfactor = self.dev)

#        self.hull_d = kamakmdev.Kamakmdev(self.datas[0], period=40, dev_period = 40, devfactor = self.dev)
#        self.hull_d.plotinfo.plot = False

        self.hull_d = mfistddev.MFIStdDev(self.datas[0], MFI_len=50, dev_period = 50, devfactor = self.dev)

        self.ssma2 = bt.indicators.SMMA(self.datas[0], period=200)

#        self.atr = bt.indicators.ATR(self.datas[0])
#
#        self.roc = bt.indicators.MAROC100(self.datas[0], ROC_len=5 , SMA_len = 21)
        
        # Variables
        self.slp1 = 2.5
        self.avg1 = 50
        self.stp = 20
        self.ltp = 100
#        self.atr_trad = 10
        
        self.trad_roe2 = 0

        self.last_long_entry = 0
        self.last_short_entry = 0
        
        self.elong_counter = 0
        self.eshort_counter = 0
        
        self.long_avg_counter = 0
        
        self.elsl_counter = 0
        self.essl_counter = 0
        
        self.trad_roe = 0
        self.il = True
        self.t_el = 0
        
        # Custom Position Counting
        self.elong_positions = 0
        self.eshort_positions = 0
        
        self.io = 0
        self.t_tl = 0
        self.t_c = 0
        
        self.trad_l1 = bt.ind.crossover.CrossUp(self.dataclose, self.hull_d.lines.b_dev) 
        self.trad_s1 = bt.ind.crossover.CrossDown(self.dataclose, self.hull_d.lines.t_dev) 
        self.trad_l2 = bt.ind.crossover.CrossDown(self.dataclose, self.hull_d.lines.t_dev) 
        self.trad_s2 = bt.ind.crossover.CrossUp(self.dataclose, self.hull_d.lines.b_dev)
        
        self.trad_l1.plotinfo.plot = False
        self.trad_s1.plotinfo.plot = False
        self.trad_l2.plotinfo.plot = False
        self.trad_s2.plotinfo.plot = False
        
    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enougth cash
        if order.status in [order.Completed]:
            lolol = 3
#            if order.isbuy():
#                self.log(
#                    'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
#                    (order.executed.price,
#                     order.executed.value,
#                     order.executed.comm))
#
#                self.buyprice = order.executed.price
#                self.buycomm = order.executed.comm
#                self.log(self.position.size)
                
#            else:  # Sell
#                self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
#                         (order.executed.price,
#                          order.executed.value,
#                          order.executed.comm))
#                self.log(self.position.size)


#            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        # Write down: no pending order
        self.order = None
        
    def notify_trade(self, trade):
        if not trade.isclosed:
            return

        self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f, POSITION OPEN %d' %
                 (trade.pnl, trade.pnlcomm,self.position.size))
        Trad_roe = trade.pnlcomm/trade.price*100
        trad_len = trade.barlen
        self.log('ROE - %.2f ' % Trad_roe)
        self.log(' ')
        self.trad_roe2 += Trad_roe
        self.t_tl += trad_len
#        data_trades = self._trades[data][0]
#        print(data_trades[self.io])
#        self.io += 1
        
    def next(self):
        
        # Simply log the closing price of the series from the reference
        #self.log("Close - %.2f | LE - %s | SE - %s " % (self.dataclose[0], self.last_long_entry, self.last_short_entry))
       # self.log('%.2f' % self.hull1.lines.hma[-1])
        # Check if an order is pending ... if yes, we cannot send a 2nd one
        if self.order:
            return

        self.t_c += 1        
#        self.slp1 = self.atr.lines.atr4[0]*(5 - (2 * abs(self.roc.lines.slp20[0])))
#        self.stp = 5 * self.atr.lines.atr4[0]*(5 + (2 * self.roc.lines.slp20[0]))
#        self.ltp = 5 * self.atr.lines.atr4[0]*(5 + (2 * self.roc.lines.slp20[0]))
#        self.hull_d.params.devfactor = ( 3 + self.atr.lines.atr3[0])
            
        #Trade Cond
        shull_tradable = self.dataclose[0] < self.ssma2.lines.smma[0]
        lhull_tradable = self.dataclose[0] > self.ssma2.lines.smma[0]
#        shull_tradable = self.ssma.lines.hma[0] > self.ssma1.lines.hma[0]
#        lhull_tradable = self.ssma.lines.hma[0] < self.ssma1.lines.hma[0]

        # ATR Trigger Calculation
#        long = False
#        short = False
#        
#        if self.trad_l1.l.cross == 1:
#            long = True
#        elif self.trad_l2.l.cross == 1:
#            long = True
#        elif self.trad_s1.l.cross == 1:
#            short = True
#        elif self.trad_s2.l.cross == 1:
#            short = True
            
        # HullMA Trigger Calculation
#        lhull_tradable = True
#        shull_tradable = True
#        if self.hull1.lines.hma[0] > self.hull1.lines.hma[-5]:
#            hull_tradable = True
#        else:
#            hull_tradable = False
        # Avg Trigger Conditions
#        long_avg = self.elong_positions == 1 and self.dataclose[0] < self.last_long_entry*((100 - self.avg1)/100) 
#        short_avg = self.short_positions == -1 and self.dataclose[0] > self.last_short_entry*((100 + self.avg1)/100)
        
        # Stoploss Trigger Conditions
        long_sl = self.position.size == 1 and self.dataclose[0] < self.last_long_entry*((100 - self.slp1)/100)
        short_sl = self.position.size == -1 and self.dataclose[0] > self.last_short_entry*((100 + self.slp1)/100)

#         Take Profit Trigger Conditions
        short_tp = self.position.size == -1 and self.dataclose[0] < self.last_short_entry*((100 - self.stp)/100)
        long_tp = self.position.size == 1 and self.dataclose[0] > self.last_long_entry*((100 + self.ltp)/100)

        # Elong Trigger Conditions
        e_long = ( self.hull_d.lines.mfi[0] < self.hull_d.lines.b_dev[0] and ( self.il or self.elong_positions == -1) and lhull_tradable and (not ( short_tp or short_sl)) )
        e_short = ( self.hull_d.lines.mfi[0] > self.hull_d.lines.t_dev[0] and ( self.il or self.elong_positions == 1) and shull_tradable and (not (long_tp or long_sl)) ) 
#        
#        # Close Conditions
#        e_long_close =  (self.stoch.lines.percDSlow[0] > self.OBstoch_value ) and self.elong_positions == 1 and (not long_tp )
#        e_short_close =  (self.stoch.lines.percDSlow[0] < self.OSstoch_value ) and self.eshort_positions == -1 and (not short_tp )
#        
        
#        # Stoch counter
#        if self.stoch.lines.percDSlow[0] > self.OBstoch_value:
#            self.OB_stoch_count += 1
#        elif self.stoch.lines.percDSlow[0] < self.OSstoch_value:
#            self.OS_stoch_count += 1
#        

            
        # Stoploss Trigger
        if long_sl:
            self.log('LONG STOP, %.2f' % self.dataclose[0])
#            self.elong_positions = -1
            self.elsl_counter += 1
            self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100 
#            self.last_long_entry = 200000
            # Keep track of the created order to avoid a 2nd order
            self.il = True
            self.order = self.close()
        
        elif short_sl:
            self.log('SHORT STOP, %.2f' % self.dataclose[0])
            self.essl_counter += 1
            self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100
#            self.last_short_entry = 0
#            self.elong_positions = -1
            self.il = True
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()     

        # Takeprofit Trigger
        if long_tp:
            self.log('LONG TP, %.2f' % self.dataclose[0])
#            self.elong_positions = 0
#            self.elsl_counter += 1
            self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100 
#            self.last_long_entry = 200000
            self.il = True

            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()
        
        elif short_tp:
            self.log('SHORT TP, %.2f' % self.dataclose[0])
            self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100
#            self.last_short_entry = 0
#            self.essl_counter += 1
#            self.eshort_positions = 0
            self.il = True

            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()  
            
#        if long_avg:
#            self.log('E_LONG AVG, %.2f' % self.dataclose[0])
#            # Keep track of the created order to avoid a 2nd order
#            self.order = self.buy()
#            self.long_avg_counter += 1
#            self.elong_positions += 1
            
        
#        elif short_avg:
#            self.log('SHORT AVG, %.2f' % self.dataclose[0])
#            # Keep track of the created order to avoid a 2nd order
#            self.order = self.sell() 
#            self.short_avg_counter += 1
#            self.short_positions -= 1       
        
#        # Check if we are in the market
#        if self.position.size == 0:
        
        # Not yet ... we MIGHT BUY if ...
        if e_long:

            self.log('E_LONG, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
            self.elong_counter += 1
            self.elong_positions = 1
            self.last_long_entry = self.dataclose[0]
            if self.il:
                self.last_short_entry = self.dataclose[0]            
            self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100 
            self.order = self.close()
            # Keep track of the created order to avoid a 2nd order
            self.order = self.buy()
            self.il = False
        
        elif  e_short:
            
            self.log('E_SHORT, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
            self.eshort_counter += 1
            self.elong_positions = -1
            self.last_short_entry = self.dataclose[0]
            if self.il:
                self.last_long_entry = self.dataclose[0]
            self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100
            
            self.order = self.close()
            # Keep track of the created order to avoid a 2nd order
            self.order = self.sell()
            self.il = False
            
#        i =  list(range(0, len(self.datas)))
#        for (d,j) in zip(self.datas,i):
#            if len(d) == (d.buflen()-1):
#                close = self.close(d,exectype=bt.Order.Market)
#                print(close)
                
                
#        else:
#
#            if  e_long_close:
#                # SELL, SELL, SELL!!! (with all possible default parameters)
#                self.log('E_LONG CLOSE, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
#                self.elong_positions = 0
#                self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100 
##                self.last_long_entry = 200000
#                self.log(self.trad_roe)
#                # Keep track of the created order to avoid a 2nd order
#                self.order = self.close()
#            
#            elif  e_short_close:
#                # SELL, SELL, SELL!!! (with all possible default parameters)
#                self.log('E_SHORT CLOSE, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
#                self.eshort_positions = 0
#                self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100
##                self.last_short_entry = 0
#                # Keep track of the created order to avoid a 2nd order
#                self.order = self.close()


val = 0
lol = 0
t_el = 0
t_es = 0
#montha = [4,5,6,7,8,9,10,11,12]
montha = [7]
for i in montha:
        
    if __name__ == '__main__':
        # Create a cerebro entity
        cerebro = bt.Cerebro()
    
    #    strats = cerebro.optstrategy(
    #        BTCUSD_5,
    #        maperiod=range(10, 31))
        
        # Add a strategy
        cerebro.addstrategy(BTCUSD_5)
       
        # Datas are in a subfolder of the samples. Need to find where the script is
        # because it could have been called from anywhere
        modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
        datapath = os.path.join(modpath, 'data\BTCUSD-all-5M.csv')
#        datapath = os.path.join(modpath, 'data\BTCUSD-17-5M-V.csv')
    
        # Create a Data Feed
        data = btfeeds.GenericCSVData(
        dataname= datapath,
#        fromdate=datetime.datetime(2018, 1, 1),
#        todate=datetime.datetime(2018, 4, 2),
#        fromdate=datetime.datetime(2017, 4, 1),
#        todate=datetime.datetime(2017, 12, 29),    
        fromdate=datetime.datetime(2017, 4, 3),
        todate=datetime.datetime(2018, 5, 30),
    
#        nullvalue=0,
    
        dtformat=('%d/%m/%Y %H:%M'),
        timeframe=bt.TimeFrame.Minutes, 
        compression=5,
    
    
        date=-1,
        datetime=0,
        high=2,
        low=3,
        open=1,
        close=4,
        volume=5,
        openinterest=-1
        )
    
#        # Create a Data Feed
#        data1 = btfeeds.GenericCSVData(
#        dataname= datapath2,
#        fromdate=datetime.datetime(2017, 4, 1),
#        todate=datetime.datetime(2017, 12, 30),
#    
#        nullvalue=0,
#    
#        dtformat=('%d/%m/%Y %H:%M'),
#        timeframe=bt.TimeFrame.Minutes, 
#        compression=360,
#    
#    
#        date=-1,
#        datetime=0,
#        high=2,
#        low=3,
#        open=1,
#        close=4,
#        volume=-1,
#        openinterest=-1
#        )
#    
#        data1.plotinfo.plot = False
        # Add the Data Feed to Cerebro
        cerebro.adddata(data)
#        cerebro.adddata(data1)
        
       
        def printSQN(analyzer):
            sqn = round(analyzer.sqn,2)
            print('SQN: {}'.format(sqn))
    
        def printMDD(analyzer):
            mdd_p = round(analyzer.max.drawdown,2)
            mdd_n = round(analyzer.max.moneydown,2)
            print('MDD: {} % - {} $ '.format(mdd_p,mdd_n ))
        
            
        # Set our desired cash start
        cerebro.broker.setcash(100000.0)     
    
        # Add a FixedSize sizer according to the stake
        cerebro.addsizer(bt.sizers.FixedSize, stake=1)
    
        # Set the commission - 0.1% ... divide by 100 to remove the %
        cerebro.broker.setcommission(commission=0.002)
    
        name = os.path.splitext(os.path.basename(__file__))[0]
        
        writeFile = open("Results/"+str(name)+".csv", "w")
        cerebro.addwriter(bt.WriterFile, out = writeFile, csv=True, rounding=2)
        
        # Set the Slippage
    #    cerebro.broker.set_slippage_fixed(0)
    
      #  bt.observers.Trades.plotlines.pnlplus.color = 'blue'
     #   bt.observers.Trades.plotlines.pnlminus.marker = 'o'
        print('Results for Month - %d' % i)
        # Print out the starting conditions
        print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())
        
        # Analyzer
    
        cerebro.addanalyzer(bt.analyzers.SQN, _name="sqn")
        cerebro.addanalyzer(bt.analyzers.DrawDown, _name="drawdown")
        cerebro.addanalyzer(bt.analyzers.Transactions, _name="transactions")
        
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, _name="a_ol")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, filter='short',_name="s_ol")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, filter='long',_name="l_ol")
        
        # Run over everything
        strategies = cerebro.run()
        firstStrat = strategies[0]
        

        # Print out the final result
        print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())
        
        val += cerebro.broker.getvalue()
        t_el += firstStrat.elong_counter
        t_es += firstStrat.eshort_counter
        
        # print the analyzers 
        firstStrat.analyzers.a_ol.print()
        firstStrat.analyzers.l_ol.print()
        firstStrat.analyzers.s_ol.print()
        printSQN(firstStrat.analyzers.sqn.get_analysis())
        print('')

        printMDD(firstStrat.analyzers.drawdown.get_analysis())
    #    print(firstStrat.analyzers.transactions.get_analysis())
    
        print("El Stop Hits - {} | ES Stop Hits - {}" 
              .format(firstStrat.elsl_counter, firstStrat.essl_counter,
                      ))
#        print("SL - {}, Long TP - {}, Short TP - {}, MA1 - {}, MA2 - {}, Dev - {}" 
#          .format(firstStrat.slp1, firstStrat.ltp,
#                  firstStrat.stp, firstStrat.ma1,
#                  firstStrat.ma2, firstStrat.dev,))
        print('')
        print('ROE - %.2f | Net- %.2f | Total Longs - %d' % ((firstStrat.trad_roe -(firstStrat.elong_counter+firstStrat.eshort_counter)*0.4), (cerebro.broker.getvalue() - 100000), t_el))
        lol += firstStrat.trad_roe
        print('SLP - %.2f | STP- %.2f | LTP - %.2f | Dev - %.2f' % (firstStrat.slp1, firstStrat.stp, firstStrat.ltp, firstStrat.dev))
        print("Total Bars - %d, Trade Bars - %d, Time Open  - %s" % ((firstStrat.t_c), firstStrat.t_tl, (str(int(firstStrat.t_tl/firstStrat.t_c*100)) + '%') ))

    #    firstStrat._trades[data][0].print()
#        print(round(val-100000*(i-3),2))
        print(' ')
        
        cerebro.plot()
        print('ROE - %.2f' % firstStrat.trad_roe2)
#print(round(val-900000,2))
print(lol)
writeFile.close()

import pandas as pd
import numpy as np

df = pd.read_csv( 'Results/'+str(name)+".csv", skiprows=2, parse_dates = [0] , dayfirst=True, 
                     usecols=[ 3 ,16 ,25])

df.columns = ['Date','value','ROE']

df = df.dropna(subset=['value'])

pd.to_datetime(df['Date'], format='%Y/%m/%d %H:%M')

df.index = pd.to_datetime(df['Date'])
df.drop(['Date'], axis = 1)
ohlc_dict = {'value':'last', 'ROE':'sum'}

df = df.resample('1W', how=ohlc_dict)
cols=['value', 'ROE']  
df = df[cols]
df = df.T
df.to_csv('Results/BCH-MFI-W.csv')

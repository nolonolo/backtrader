from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import datetime  # For datetime objects
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])

# Import the backtrader platform
import backtrader as bt
import backtrader.feeds as btfeeds
import backtrader.indicators as btind
from backtrader.indicators import hullrb
from backtrader.indicators import hma1
import backtrader.analyzers as btanalyzers
# Create a Stratey
class BTCUSD_5(bt.Strategy):
    
    params = (
        ('maperiod', 550),
    )

    
    def log(self, txt, dt=None):
        #''' Logging function for this strategy'''
        dt = dt or self.datas[0].datetime.datetime(0)
        print('%s' % (txt))

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.dataclose = self.datas[0].close

        # To keep track of pending orders and buy price/commission
        self.order = None
        self.buyprice = None
        self.buycomm = None

        # Add a MovingAverageSimple indicator
        self.sma = btind.SimpleMovingAverage(
            self.datas[0], period=self.params.maperiod)

        
        self.hull_en = hullrb.HullMovingRB(self.datas[0], period=250, perc = 6.5)
        self.hull_en.plotinfo.plot = False
        self.hull = bt.indicators.HullMovingAverage(self.datas[0], period=550)
        self.hull.plotinfo.plot = False
        
       
        self.stoch = bt.indicators.StochasticFull(self.datas[0])
        self.stoch.plotinfo.plot = False
        self.ssma = bt.indicators.SmoothedMovingAverageEnvelope(self.datas[0], period=250, perc = 1)
        self.ssma.plotinfo.plot = False

        self.LOverBought = 93      
        self.LOverSold = 13
        self.SOverBought = 89
        self.SOverSold = 7
        self.slp1 = 7.5
        #slp2 = 5
        self.slp4 = 5
        self.i = 500
        #slp5 = 5
        #tpp =  50 
        #tsp =  50
        self.avg1 = 2.5
        #avg2 = 2.5
        self.last_long_entry = [0]
        self.last_short_entry = [0]
            
        
    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enougth cash
        if order.status in [order.Completed]:
            if order.isbuy():
                self.log(
                    'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                    (order.executed.price,
                     order.executed.value,
                     order.executed.comm))

                self.buyprice = order.executed.price
                self.buycomm = order.executed.comm
                #self.log(self.position.size)
                

                
            else:  # Sell
                self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                         (order.executed.price,
                          order.executed.value,
                          order.executed.comm))
                #self.log(self.position.size)


            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        # Write down: no pending order
        self.order = None
        
    def notify_trade(self, trade):
        if not trade.isclosed:
            return

        self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f' %
                 (trade.pnl, trade.pnlcomm))

    def next(self):
        
        # Simply log the closing price of the series from the reference
        #self.log("Close - %.2f | LE - %s | SE - %s " % (self.dataclose[0], self.last_long_entry, self.last_short_entry))

        # Check if an order is pending ... if yes, we cannot send a 2nd one
        if self.order:
            return
        
        # Avg Trigger Conditions
        long_avg = self.position.size == 1 and self.dataclose[0] < self.last_long_entry*((100 - self.avg1)/100)
        short_avg = self.position.size == -1 and self.dataclose[0] > self.last_short_entry*((100 + self.avg1)/100)
        
        # Stoploss Trigger Conditions
        long_sl = self.position.size > 0 and self.dataclose[0] < self.last_long_entry*((100 - self.slp1)/100)
        short_sl = self.position.size < 0 and self.dataclose[0] > self.last_short_entry*((100 + self.slp1)/100)
        
        # Adpative Stoploss Trigger Conditions
        Short_adp_sl = self.dataclose[0] > self.hull_en.lines.top[0] and self.position.size <= -1
        Long_adp_sl = self.hull.lines.hma[0] > self.hull_en.lines.hma[0] and self.position.size >= -1
        
        # Long Trigger Conditions
        long = (self.stoch.lines.percDSlow[0] < self.LOverSold ) and self.dataclose[0] > self.ssma.lines.bot[0]
        closelong = (self.stoch.lines.percDSlow[0] > self.LOverBought and self.position.size > 0)
        
        # Elong Trigger Conditions
        e_long = self.dataclose[0] < self.hull_en.lines.bot[0]
        e_long_close = self.dataclose[0] > self.hull_en.lines.top[0] and self.position.size == 1

        # Short Trigger Conditions
        short = (self.stoch.lines.percDSlow[0] > self.SOverBought ) and self.dataclose[0] < self.ssma.lines.top[0]
        closeshort = (self.stoch.lines.percDSlow[0] < self.SOverSold) and self.position.size < 0        
        
        # Reassign Stoplosses
        if Short_adp_sl:
            self.slp1 = self.slp4
        else:
            self.slp1 = self.slp1
        
        if Long_adp_sl:
            self.slp1 = self.slp4
        else:
            self.slp1 = self.slp1        
        
       
        # Averaeging Trigger
        
        if long_avg:
            self.log('Long Avg, %.2f' % self.dataclose[0])
            # Keep track of the created order to avoid a 2nd order
            self.order = self.buy()
        
        elif short_avg:
            self.log('Long Avg, %.2f' % self.dataclose[0])
            # Keep track of the created order to avoid a 2nd order
            self.order = self.sell()            

        # Stoploss Trigger
        if long_sl:
            self.log('Long Avg, %.2f' % self.dataclose[0])
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()
        
        elif short_sl:
            self.log('Long Avg, %.2f' % self.dataclose[0])
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()            
        
        # Check if we are in the market
        if self.position.size == 0:

            # Not yet ... we MIGHT BUY if ...
            if e_long:

                # BUY, BUY, BUY!!! (with all possible default parameters)
                self.log('Elong, %.2f' % self.dataclose[0])
                
                # Keep track of the created order to avoid a 2nd order
                self.order = self.buy()
            
            elif long:

                self.log('Long, %.2f' % self.dataclose[0])
                
                # Keep track of the created order to avoid a 2nd order
                self.order = self.buy()
                
                # Record Entry Position
                self.last_long_entry = self.dataclose[0]
                #self.log(self.last_long_entry)                


            elif short:

                self.log('Short, %.2f' % self.dataclose[0])
                
                # Keep track of the created order to avoid a 2nd order
                self.order = self.sell()                   

                self.last_short_entry = self.dataclose[0]
                #self.log(self.last_short_entry)

        else:

            if e_long_close:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('Elong Close, %.2f' % self.dataclose[0])

                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()
                
            elif closelong:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('Long Close, %.2f' % self.dataclose[0])
                
                self.last_long_entry = 0

                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()

            elif closeshort:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('Close Short, %.2f' % self.dataclose[0])
                self.last_short_entry = 0

                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()


if __name__ == '__main__':
    # Create a cerebro entity
    cerebro = bt.Cerebro()

    # Add a strategy
    cerebro.addstrategy(BTCUSD_5)
    
    # Datas are in a subfolder of the samples. Need to find where the script is
    # because it could have been called from anywhere
    modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
    datapath = os.path.join(modpath, '5min.csv')

    # Create a Data Feed
    data = btfeeds.GenericCSVData(
    dataname= datapath,
    fromdate=datetime.datetime(2017, 12, 16),
    todate=datetime.datetime(2017, 12, 27),

    nullvalue=0.0,

    dtformat=('%d/%m/%Y %H:%M'),
    timeframe=bt.TimeFrame.Minutes, 
    compression=1,


    date=-1,
    datetime=0,
    high=2,
    low=3,
    open=1,
    close=4,
    volume=5,
    openinterest=-1
    )
    
        
    # Add the Data Feed to Cerebro
    cerebro.adddata(data)
    
    def printTradeAnalysis(analyzer):

        #Get the results we are interested in
        total_open = analyzer.total.open
        total_closed = analyzer.total.closed
        total_won = analyzer.won.total
        total_lost = analyzer.lost.total
        pnl_avg = analyzer.pnl.net.average
        lose_streak = analyzer.streak.lost.longest
        pnl_net = round(analyzer.pnl.net.total,2)
        strike_rate = (total_won / total_closed) * 100
        pf = analyzer.pnl.gross.total/(analyzer.pnl.gross.total-analyzer.pnl.net.total)
        avg_trade_time = analyzer.len.average * 5/60
        
        #Designate the rows
        h1 = ['Total Open', 'Total Closed', 'Total Won', 'Total Lost', 'PF']
        h2 = ['Strike Rate','Avg Win', 'Losing Streak', 'PnL Net', 'Avg Time']
        r1 = [total_open, total_closed,total_won,total_lost,round(pf,2)]
        r2 = [round(strike_rate,2), round(pnl_avg,2), lose_streak, pnl_net,' {} Hrs '.format(avg_trade_time)]
        #Check which set of headers is the longest.
        if len(h1) > len(h2):
            header_length = len(h1)
        else:
            header_length = len(h2)
            #Print the rows
            print_list = [h1,r1,h2,r2]
            row_format ="{:<15}" * (header_length + 1)
            print("Trade Analysis Results:")
            for row in print_list:
                  print(row_format.format('',*row))
 
    def printSQN(analyzer):
        sqn = round(analyzer.sqn,2)
        print('SQN: {}'.format(sqn))

    def printMDD(analyzer):
        mdd = round(analyzer.max.moneydown,2)
        print('MDD: {}'.format(mdd))

    # Set our desired cash start
    cerebro.broker.setcash(100000.0)     

    # Add a FixedSize sizer according to the stake
    cerebro.addsizer(bt.sizers.FixedSize, stake=1)

    # Set the commission - 0.1% ... divide by 100 to remove the %
    cerebro.broker.setcommission(commission=0.001)

    # Print out the starting conditions
    print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())
    
    # Analyzer
    cerebro.addanalyzer(bt.analyzers.TradeAnalyzer, _name="ta")
    cerebro.addanalyzer(bt.analyzers.SQN, _name="sqn")
    cerebro.addanalyzer(bt.analyzers.DrawDown, _name="drawdown")

    # Run over everything
    strategies = cerebro.run()
    firstStrat = strategies[0]
    
    # Print out the final result
    print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())
    # print the analyzers 
    printTradeAnalysis(firstStrat.analyzers.ta.get_analysis())
    printSQN(firstStrat.analyzers.sqn.get_analysis())
    printMDD(firstStrat.analyzers.drawdown.get_analysis())
    
#    cerebro.plot(style='candlestick', barup='green', bardown='red')

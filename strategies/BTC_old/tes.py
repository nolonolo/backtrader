import pandas as pd
import numpy as np

df = pd.read_csv( '123.csv',parse_dates={'DateTime': ['Date', 'Time']}, dayfirst = True,na_values=[np.nan],
             usecols=['Date', 'Time', 'open','high','low','close']).set_index('DateTime')

# Created a dictionary to tell Pandas how to re-sample, if this isn't in place it will re-sample each column separately 

ohlc_dict = {'open':'first', 'high':'max', 'low':'min', 'close': 'last'}

# Resample to 5Min (this format is needed) as per ohlc_dict, then remove any line with a NaN
df = df.resample('1Min', how=ohlc_dict)

# Resample mixes the columns so lets re-arrange them 
cols=['open', 'high', 'low', 'close']  
df = df[cols]
df.to_csv("5min.csv")
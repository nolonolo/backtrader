from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import datetime  # For datetime objects
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])
import warnings
warnings.filterwarnings("ignore")

# Import the backtrader platform
import backtrader as bt
import backtrader.feeds as btfeeds
import backtrader.indicators as btind
from backtrader.indicators import hullrb
from backtrader.indicators import hma1
import backtrader.analyzers as btanalyzers
from backtrader.analyzers import basictradestats
# Create a Stratey
class BTCUSD_5(bt.Strategy):
    

    def log(self, txt, dt=None):
        #''' Logging function for this strategy'''
        dt = dt or self.datas[0].datetime.datetime(0)
#        print('%s' % (txt))

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.dataclose = self.datas[0].close


        # To keep track of pending orders and buy price/commission
        self.order = None
        self.buyprice = None
        self.buycomm = None

       
        self.hull_en = hullrb.HullMovingRB(self.datas[0], period=250, perc = 7.5)
  #      self.hull_en.plotinfo.plot = False

        self.hull1 = bt.indicators.HullMovingAverage(self.datas[0], period=250)
        self.hull1.plotinfo.plot = True
        
        self.atr = bt.indicators.ATR(self.datas[0])
#        self.atr.plotlines.atr2._plotskip = True
#        self.atr.plotlines.atr._plotskip= True
#        
        self.roc = bt.indicators.MAROC100(self.datas[0], ROC_len=5 , SMA_len = 21)
        
        self.stoch = bt.indicators.StochasticFull(self.datas[0], period=24, period_dslow = 4)
   #     self.stoch.plotinfo.plot = True
        self.ssma = bt.indicators.SMAEnvelope(self.datas[0], period=250, perc = 0)
     #   self.ssma.plotinfo.plot = False
#        self.ssma1 = bt.indicators.SmoothedMovingAverage(self.datas[1], period=30)



        self.LOverBought = 93
        self.LOverSold = 13
        self.SOverBought = 89
        self.SOverSold = 7
        
        self.slp1 = 2.5
        self.slp2 = 2.5
        self.slp4 = 2.5
        self.ltp = 100
        self.stp = 100
        self.elong = True
        
        self.avg1 = 10
        self.atr_trad = 0.375
        
        self.last_long_entry = 0
        self.last_short_entry = 0
        self.last_elong_entry = 0
        self.long_counter = 0
        self.short_counter = 0
        self.elong_counter = 0
        self.last_elong_entry = 0
        self.e_long_on = False
        self.trad_roe = 0
        
        self.elong_stop_counter = 0
        self.short_stop_counter = 0
        self.long_stop_counter = 0     
        self.OB_stoch_count = 0
        self.OS_stoch_count = 0
        
        # Custom Position Counting
        self.elong_positions = 0
        self.long_positions = 0
        self.short_positions = 0
        
        self.io = 0
        self.t_tl = 0
        self.t_c = 0
        
    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enougth cash
        if order.status in [order.Completed]:
#            if order.isbuy():
#                self.log(
#                    'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
#                    (order.executed.price,
#                     order.executed.value,
#                     order.executed.comm))
#
#                self.buyprice = order.executed.price
#                self.buycomm = order.executed.comm
#                #self.log(self.position.size)
#                
#
#                
#            else:  # Sell
#                self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
#                         (order.executed.price,
#                          order.executed.value,
#                          order.executed.comm))
                #self.log(self.position.size)


            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        # Write down: no pending order
        self.order = None
        
    def notify_trade(self, trade):
        if not trade.isclosed:
            return

        self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f, POSITION OPEN %d' %
                 (trade.pnl, trade.pnlcomm,self.position.size))
        Trad_roe = trade.pnlcomm/trade.price*100
        trad_len = trade.barlen
        self.log('ROE - %.2f ' % Trad_roe)
        self.log(' ')
        self.trad_roe += Trad_roe
        self.t_tl += trad_len
#        data_trades = self._trades[data][0]
#        print(data_trades[self.io])
#        self.io += 1
        
    def next(self):
        
        # Simply log the closing price of the series from the reference
        #self.log("Close - %.2f | LE - %s | SE - %s " % (self.dataclose[0], self.last_long_entry, self.last_short_entry))
       # self.log('%.2f' % self.hull1.lines.hma[-1])
        # Check if an order is pending ... if yes, we cannot send a 2nd one
        if self.order:
            return
       
        self.t_c += 1
        self.slp1 = self.atr.lines.atr4[0]*(5 - (2 * abs(self.roc.lines.slp20[0])))
#        self.stp = 5 * self.atr.lines.atr4[0]*(5 + (2 * self.roc.lines.slp20[0]))
#        self.ltp = 5 * self.atr.lines.atr4[0]*(5 + (2 * self.roc.lines.slp20[0]))
#        self.hull_d.params.devfactor = ( 3 + self.atr.lines.atr3[0])
            
        # HullMA Trigger Calculation
        hull_tradable = True
        if self.hull1.lines.hma[0] > self.hull1.lines.hma[-1]:
            hull_tradable = True
        else:
            hull_tradable = False
       

        # ATR Trigger Calculation
        atr_tradable = True
        if self.atr.lines.atr3[0] > self.atr_trad:
            atr_tradable = True
        else:
            atr_tradable = False
       
        # Stoploss Trigger Conditions
        long_sl = self.position.size > 0 and self.dataclose[0] < self.last_long_entry*((100 - self.slp1)/100)
        short_sl = self.position.size < 0 and self.dataclose[0] > self.last_short_entry*((100 + self.slp1)/100)
        e_long_sl = self.elong_positions == 1  and self.dataclose[0] < self.last_elong_entry*((100 - self.slp2)/100)
        
        # Avg Trigger Conditions
        long_avg = self.long_positions == 1 and self.dataclose[0] < self.last_long_entry*((100 - self.avg1)/100) and self.e_long_on == False and (not long_sl )
        short_avg = self.short_positions == -1 and self.dataclose[0] > self.last_short_entry*((100 + self.avg1)/100) and (not short_sl )
        
        # Long Trigger Conditions
        long = (self.stoch.lines.percDSlow[0] < self.LOverSold ) and self.dataclose[0] > self.ssma.lines.bot[0] and hull_tradable and atr_tradable and self.long_positions == 0
        closelong = self.stoch.lines.percDSlow[0] > self.LOverBought and self.long_positions > 0 and (not long_sl )
        
        # Elong Trigger Conditions,.
        e_long = self.dataclose[0] < self.hull_en.lines.bot[0] and self.elong_positions == 0 and self.elong 
        e_long_close =  (self.stoch.lines.percDSlow[0] > 87.0 ) and self.elong_positions == 1 

        # Short Trigger Conditions
        short = (self.stoch.lines.percDSlow[0] > self.SOverBought ) and self.dataclose[0] < self.ssma.lines.top[0] and hull_tradable and atr_tradable and self.short_positions == 0
        closeshort = (self.stoch.lines.percDSlow[0] < self.SOverSold) and self.short_positions < 0 and (not short_sl )
        
#        Reassign Stoplosses
#        self.slp1 = self.slp2
#        if Short_adp_sl:
#            self.slp1 = self.slp4
#        else:
#            self.slp1 = self.slp1
#        
#        if Long_adp_sl:
#            self.slp1 = self.slp4
#        else:
#            self.slp1 = self.slp1        
        
        # Stoch counter
        if self.stoch.lines.percDSlow[0] > self.LOverBought:
            self.OB_stoch_count += 1
        elif self.stoch.lines.percDSlow[0] < self.SOverSold:
            self.OS_stoch_count += 1
        
      
        if long_avg:
            self.log('LONG AVG, %.2f' % self.dataclose[0])
            # Keep track of the created order to avoid a 2nd order
            self.order = self.buy()
#            self.long_avg_counter += 1
            self.long_positions = 2
            
        
        elif short_avg:
            self.log('SHORT AVG, %.2f' % self.dataclose[0])
            # Keep track of the created order to avoid a 2nd order
            self.order = self.sell() 
#            self.short_avg_counter += 1
            self.short_positions = -2

        # Stoploss Trigger
        if long_sl:
            self.log('LONG STOP, %.2f' % self.dataclose[0])

#            if self.long_positions == 2:
#                self.trad_roe += (-(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100)*1.5
#            else:
#                 self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100
            
            self.long_stop_counter += 1
            
            self.last_long_entry = 0
            self.long_positions = 0                
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()
        
        elif short_sl:
            self.log('SHORT STOP, %.2f' % self.dataclose[0])

            self.short_stop_counter += 1


#            if self.short_positions == -2:
#                self.trad_roe += ((self.last_short_entry - self.dataclose[0])/self.last_short_entry*100)*1.5
#            else:
#                self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100          
#            
            self.short_positions = 0
            self.last_short_entry = 0
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()  
            
        if e_long_sl:
            self.log('ELONG STOP, %.2f' % self.dataclose[0])
            self.elong_stop_counter += 1
            self.last_elong_entry = 0
            self.elong_positions = 0                
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()          
        
        # Check if we are in the market
        if self.position.size == 0:

            # Not yet ... we MIGHT BUY if ...
            if e_long:

                # BUY, BUY, BUY!!! (with all possible default parameters)
                self.log('E_LONG, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                self.elong_counter += 1
                self.e_long_on = True
                self.elong_positions = 1
                self.last_elong_entry = self.dataclose[0]
                # Keep track of the created order to avoid a 2nd order
                self.order = self.buy()
                
            
            elif long:

                self.log('LONG, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                
                # Keep track of the created order to avoid a 2nd order
                self.order = self.buy()
                self.long_positions = 1
                # Record Entry Position
                self.last_long_entry = self.dataclose[0]
                self.long_counter += 1
                #self.log(self.last_long_entry)                


            elif short:

                self.log('SHORT, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                
                # Keep track of the created order to avoid a 2nd order
                self.order = self.sell()                   
                self.short_counter += 1
                self.short_positions = -1
                self.last_short_entry = self.dataclose[0]
                #self.log(self.last_short_entry)

        else:

            if  e_long_close:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('E_LONG CLOSE, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                self.e_long_on = False
                self.elong_positions = 0
#                self.trad_roe += -(self.last_elong_entry - self.dataclose[0])/self.last_elong_entry*100
                self.last_elong_entry = 0
                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()
                
            elif closelong:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('LONG CLOSE, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                
#                if self.long_positions == 2:
#                    self.trad_roe += (-(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100)*1.5
#                else:
#                    self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100
                
                self.long_positions = 0      
                self.last_long_entry = 0
                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()

            elif closeshort:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('CLOSE SHORT, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))

#                if self.short_positions == -2:
#                    self.trad_roe += ((self.last_short_entry - self.dataclose[0])/self.last_short_entry*100)*1.5
#                else:
#                    self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100          

                self.short_positions = 0
                self.last_short_entry = 0
                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()

val = 0
lol = 0
t_el = 0
t_es = 0
#montha = [4,5,6,7,8,9,10,11,12]
montha = [4]
for i in montha:
    if __name__ == '__main__':
        # Create a cerebro entity
        cerebro = bt.Cerebro()
    
        # Add a strategy
        cerebro.addstrategy(BTCUSD_5)
        
        # Datas are in a subfolder of the samples. Need to find where the script is
        # because it could have been called from anywhere
        modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
#        datapath = os.path.join(modpath, 'BTCUSD-18-5M.csv')
        datapath = os.path.join(modpath, 'BTCUSD-all-5M.csv')
        
        # Create a Data Feed
        data = btfeeds.GenericCSVData(
        dataname= datapath,
#        fromdate=datetime.datetime(2017,4, 1),
#        todate=datetime.datetime(2017, 11, 29),
        fromdate=datetime.datetime(2017, 4, 1),
        todate=datetime.datetime(2018, 5, 2),
        
        nullvalue=0.0,
    
        dtformat=('%d/%m/%Y %H:%M'),
        timeframe=bt.TimeFrame.Minutes, 
        compression=5,
    
    
        date=-1,
        datetime=0,
        high=2,
        low=3,
        open=1,
        close=4,
        volume=5,
        openinterest=-1
        )
        
#        data1 = btfeeds.GenericCSVData(
#        dataname= datapath1,
#        fromdate=datetime.datetime(2017, 4, 1),
#        todate=datetime.datetime(2017, 1, 30),
#    
#        nullvalue=0.0,
#    
#        dtformat=('%d/%m/%Y %H:%M'),
#        timeframe=bt.TimeFrame.Minutes, 
#        compression=1,
#    
#    
#        date=-1,
#        datetime=0,
#        high=2,
#        low=3,
#        open=1,
#        close=4,
#        volume=-1,
#        openinterest=-1
#        )
        

        # Add the Data Feed to Cerebro
        cerebro.adddata(data)
#        
#        cerebro.resampledata(data,timeframe=bt.TimeFrame.Minutes,
#                             compression=3)
#        cerebro.adddata(data1)
#        data1.plotinfo.plot = False
        
#        def printTradeAnalysis(analyzer):
#            avg_trade_time = analyzer.len.average * 5/60
#            r2 = [' %.2f Hrs '% (avg_trade_time)]
#            print(r2)
            
        def printSQN(analyzer):
            sqn = round(analyzer.sqn,2)
            print('SQN: {}'.format(sqn))
    
        def printMDD(analyzer):
            mdd = round(analyzer.max.moneydown,2)
            print('MDD: {}'.format(mdd))
        
    
        # Set our desired cash start
        cerebro.broker.setcash(100000.0)     
    
        # Add a FixedSize sizer according to the stake
        cerebro.addsizer(bt.sizers.FixedSize, stake=1)
    
        # Set the commission - 0.1% ... divide by 100 to remove the %
        cerebro.broker.setcommission(commission=0.0005)
 
    
        name = os.path.splitext(os.path.basename(__file__))[0]
        
        writeFile = open("/"+str(name)+".csv", "w")
        cerebro.addwriter(bt.WriterFile, out = writeFile, csv=True, rounding=2)
        
        # Set the Slippage
        cerebro.broker.set_slippage_fixed(0)
        print('Results for Month - %d' % i)
        # Print out the starting conditions
        print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())
        
        # Analyzer
        cerebro.addanalyzer(bt.analyzers.TradeAnalyzer, _name="ta")
        cerebro.addanalyzer(bt.analyzers.SQN, _name="sqn")
        cerebro.addanalyzer(bt.analyzers.DrawDown, _name="drawdown")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, _name="a_ol")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, filter='short',_name="s_ol")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, filter='long',_name="l_ol")
        
        # Run over everything
        strategies = cerebro.run()
        firstStrat = strategies[0]
        

        # Print out the final result
        print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())

        val += cerebro.broker.getvalue()
        t_el += firstStrat.elong_counter
        tol = 0
        tol = (firstStrat.long_counter + firstStrat.short_counter + firstStrat.elong_counter) * 0.4
        # print the analyzers 
        firstStrat.analyzers.a_ol.print()
        firstStrat.analyzers.l_ol.print()
        firstStrat.analyzers.s_ol.print()
        
        printSQN(firstStrat.analyzers.sqn.get_analysis())
#        print(firstStrat._trades)
        print('')
        
        printMDD(firstStrat.analyzers.drawdown.get_analysis())
    #    print(firstStrat.analyzers.transactions.get_analysis())
    
        print("Long Stop Hits - {} | Short Stop Hits - {} | Stoch Hit {} - {} Times & Hit {} - {} Times" 
              .format(firstStrat.long_stop_counter, firstStrat.short_stop_counter,
                      firstStrat.LOverBought, firstStrat.OB_stoch_count,
                      firstStrat.SOverSold, firstStrat.OS_stoch_count,))
        
        print("SL - {}, Long TP - {}, Short TP -{}, Avg - {}, ATR - {}"
          .format(firstStrat.slp1, firstStrat.ltp,
                  firstStrat.stp,  firstStrat.avg1, firstStrat.atr_trad))
        print('')
        
        print('ROE - %.2f | Net- %.2f | Total trades - %d' % ((firstStrat.trad_roe), val-100000*(i-3), t_el))
        lol += firstStrat.trad_roe
        print("Total Bars - %d, Trade Bars - %d, Time Open  - %s" % ((firstStrat.t_c), firstStrat.t_tl, (str(int(firstStrat.t_tl/firstStrat.t_c*100)) + '%') ))
#        print('Total Avgs - %d, Long Avgs - %d' % ((firstStrat.long_avg_counter), firstStrat.long_avg_counter))
    #    firstStrat._trades[data][0].print()
#        print(round(val-100000*(i-3),2))
        print(' ')
        
        cerebro.plot(style='candlesticks')
#print('ROE - %.2f' % lol)
writeFile.close()

import pandas as pd
import numpy as np

df = pd.read_csv( '/'+str(name)+".csv", skiprows=2, parse_dates = [0] , dayfirst=True, 
                     usecols=[ 3 ,16 ,25])

df.columns = ['Date','value','ROE']

df = df.dropna(subset=['value'])

pd.to_datetime(df['Date'], format='%Y/%m/%d %H:%M')

df.index = pd.to_datetime(df['Date'])
df.drop(['Date'], axis = 1)
ohlc_dict = {'value':'last', 'ROE':'sum'}

df = df.resample('1W', how=ohlc_dict)
cols=['value', 'ROE']  
df = df[cols]
df = df.T
df.to_csv('5.8-W.csv')

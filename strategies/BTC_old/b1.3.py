from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import datetime  # For datetime objects
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])

# Import the backtrader platform
import backtrader as bt
import backtrader.feeds as btfeeds
import backtrader.indicators as btind
from backtrader.indicators import hullrb
from backtrader.indicators import hma1
# Create a Stratey
class TestStrategy(bt.Strategy):
    
    params = (
        ('maperiod', 15),
    )

    
    def log(self, txt, dt=None):
        #''' Logging function for this strategy'''
        dt = dt or self.datas[0].datetime.datetime(0)
        print('%s' % (txt))

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.dataclose = self.datas[0].close

        # To keep track of pending orders and buy price/commission
        self.order = None
        self.buyprice = None
        self.buycomm = None

        # Add a MovingAverageSimple indicator
        self.sma = btind.SimpleMovingAverage(
            self.datas[0], period=self.params.maperiod)
        self.sma1 = btind.SimpleMovingAverage(
            self.datas[0], period=1)
        self.sma1.plotinfo.plot = False

        
        self.hull_en = hullrb.HullMovingRB(self.datas[0], period=250, perc = 6.5)
        bt.indicators.HullMovingAverage(self.datas[0], period=550)
        self.stoch = bt.indicators.StochasticFull(self.datas[0])
        self.ssma = bt.indicators.SmoothedMovingAverageEnvelope(self.datas[0], period=250, perc = 1)
        

        self.LOverBought = 93      
        self.LOverSold = 13
        self.SOverBought = 89
        self.SOverSold = 7
        self.slp1 = 7.5
        #slp2 = 5
        self.slp4 = 7.5
        #slp5 = 5
        #tpp =  50 
        #tsp =  50
        self.avg1 = 2.5
        #avg2 = 2.5
        self.last_long_entry = [0]
        self.last_short_entry = [0]
            
        
    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enougth cash
        if order.status in [order.Completed]:
            if order.isbuy():
                self.log(
                    'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                    (order.executed.price,
                     order.executed.value,
                     order.executed.comm))

                self.buyprice = order.executed.price
                self.buycomm = order.executed.comm
                #self.log(self.position.size)
                

                
            else:  # Sell
                self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                         (order.executed.price,
                          order.executed.value,
                          order.executed.comm))
                #self.log(self.position.size)


            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        # Write down: no pending order
        self.order = None
        
    def notify_trade(self, trade):
        if not trade.isclosed:
            return

        self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f' %
                 (trade.pnl, trade.pnlcomm))

    def next(self):
        
        # Simply log the closing price of the series from the reference
        #self.log("Close - %.2f | LE - %s | SE - %s " % (self.dataclose[0], self.last_long_entry, self.last_short_entry))

        # Check if an order is pending ... if yes, we cannot send a 2nd one
        if self.order:
            return
        
        # Avg Conditions
        long_avg = self.position.size == 1 and self.dataclose[0] < self.last_long_entry*((100 - self.avg1)/100)
        short_avg = self.position.size == -1 and self.dataclose[0] > self.last_short_entry*((100 + self.avg1)/100)
        
        # Stoploss Conditions
        long_sl = self.position.size > 0 and self.dataclose[0] < self.last_long_entry*((100 - self.slp1)/100)
        short_sl = self.position.size < 0 and self.dataclose[0] > self.last_short_entry*((100 + self.slp1)/100)
                
        # Long Trigger Conditions
        long = (self.stoch.lines.percDSlow[0] < self.LOverSold ) and self.dataclose[0] > self.ssma.lines.bot[0]
        closelong = (self.stoch.lines.percDSlow[0] > self.LOverBought and self.position.size > 0)
        
        # Elong Trigger Conditions
        e_long = self.dataclose[0] < self.hull_en.lines.bot[0]
        e_long_close = self.dataclose[0] > self.hull_en.lines.top[0] and self.position.size == 1

        # Short Trigger Conditions
        short = (self.stoch.lines.percDSlow[0] > self.SOverBought ) and self.dataclose[0] < self.ssma.lines.top[0]
        closeshort = (self.stoch.lines.percDSlow[0] < self.SOverSold) and self.position.size < 0        
        
        # Averaeging Trigger
        
        if long_avg:
            self.log('Long Avg, %.2f' % self.dataclose[0])
            # Keep track of the created order to avoid a 2nd order
            self.order = self.buy()
        
        elif short_avg:
            self.log('Long Avg, %.2f' % self.dataclose[0])
            # Keep track of the created order to avoid a 2nd order
            self.order = self.sell()            

        # Stoploss Trigger
        if long_sl:
            self.log('Long Avg, %.2f' % self.dataclose[0])
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()
        
        elif short_sl:
            self.log('Long Avg, %.2f' % self.dataclose[0])
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()            
        
        # Check if we are in the market
        if self.position.size == 0:

            # Not yet ... we MIGHT BUY if ...
            if e_long:

                # BUY, BUY, BUY!!! (with all possible default parameters)
                self.log('Elong, %.2f' % self.dataclose[0])
                
                # Keep track of the created order to avoid a 2nd order
                self.order = self.buy()
            
            elif long:

                self.log('Long, %.2f' % self.dataclose[0])
                
                # Keep track of the created order to avoid a 2nd order
                self.order = self.buy()
                
                # Record Entry Position
                self.last_long_entry = self.dataclose[0]
                #self.log(self.last_long_entry)                


            elif short:

                self.log('Short, %.2f' % self.dataclose[0])
                
                # Keep track of the created order to avoid a 2nd order
                self.order = self.sell()                   

                self.last_short_entry = self.dataclose[0]
                #self.log(self.last_short_entry)

        else:

            if e_long_close:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('Elong Close, %.2f' % self.dataclose[0])

                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()
                
            elif closelong:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('Long Close, %.2f' % self.dataclose[0])
                
                self.last_long_entry = 0

                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()

            elif closeshort:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('Close Short, %.2f' % self.dataclose[0])
                self.last_short_entry = 0

                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()


if __name__ == '__main__':
    # Create a cerebro entity
    cerebro = bt.Cerebro()

    # Add a strategy
    cerebro.addstrategy(TestStrategy)
    
    # Datas are in a subfolder of the samples. Need to find where the script is
    # because it could have been called from anywhere
    modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
    datapath = os.path.join(modpath, '5min.csv')

    # Create a Data Feed
    data = btfeeds.GenericCSVData(
    dataname= datapath,
    fromdate=datetime.datetime(2017, 12, 16),
    todate=datetime.datetime(2017, 12, 27),

    nullvalue=0.0,

    dtformat=('%d/%m/%Y %H:%M'),
    timeframe=bt.TimeFrame.Minutes, 
    compression=1,


    date=-1,
    datetime=0,
    high=2,
    low=3,
    open=1,
    close=4,
    volume=5,
    openinterest=-1
    )
    
        
    # Add the Data Feed to Cerebro
    cerebro.adddata(data)

    # Set our desired cash start
    cerebro.broker.setcash(100000.0)     

    # Add a FixedSize sizer according to the stake
    cerebro.addsizer(bt.sizers.FixedSize, stake=1)

    # Set the commission - 0.1% ... divide by 100 to remove the %
    cerebro.broker.setcommission(commission=0.001)

    # Print out the starting conditions
    print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())

    # Run over everything
    cerebro.run()

    # Print out the final result
    print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())
    
    cerebro.plot(style='candlestick', barup='green', bardown='red')

import pandas as pd
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])
import numpy as np

modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
datapath = os.path.join(modpath, 'H:/AlgoTrad/Backtesting/BTCUSD/BTC USD data/Data Bitfinex BTCUSD Raw 2017-12-18 10-33-01.000_2018-01-15 10-33-01.000.csv')


df = pd.read_csv(datapath, sep = ',', parse_dates = True, infer_datetime_format='%Y-%m-%d %H:%M:%S', na_values=[np.nan],
                 usecols=['timestamp', 'price'], index_col = 0)


# Created a dictionary to tell Pandas how to re-sample, if this isn't in place it will re-sample each column separately 

ohlc_dict = {'open':'first', 'high':'max', 'low':'min', 'close': 'last'}

# Resample to 5Min (this format is needed) as per ohlc_dict, then remove any line with a NaN
df = df.resample('1Min', how=ohlc_dict)

# Resample mixes the columns so lets re-arrange them 
cols=['open', 'high', 'low', 'close']  
df = df[cols]

# Write out to CSV
df.to_csv("BTCUSD-17-1M.csv")
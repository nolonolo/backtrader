from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import datetime  # For datetime objects
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])
import warnings
warnings.filterwarnings("ignore")

# Import the backtrader platform
import backtrader as bt
import backtrader.feeds as btfeeds
#import backtrader.indicators as btind
#import backtrader.analyzers as btanalyzers
#from backtrader.analyzers import basictradestats
# Create a Stratey
class BTCUSD_5(bt.Strategy):
    
    params = dict(
        cheat=False,
    )
    def log(self, txt, dt=None):
        #''' Logging function for this strategy'''
        dt = dt or self.datas[0].datetime.datetime(0)
#        print('%s' % (txt))

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.dataclose = self.datas[0].close
        self.dataopen = self.datas[0].open
        
        # To keep track of pending orders and buy price/commission
        self.order = None
        self.buyprice = None
        self.buycomm = None

        self.diff = 15
        self.slp = 1
        self.stp = 1.75
        self.ltp = 1.75
        
        self.last_long_entry = 0
        self.last_short_entry = 0
        self.long_counter = 0
        self.short_counter = 0
        self.trad_roe = 0

        self.short_stop_counter = 0
        self.long_stop_counter = 0     
        self.il = True
        
        # Custom Position Counting

        self.long_positions = 0
        self.short_positions = 0
        
        self.io = 0
        self.t_tl = 0
        self.t_c = 0
        
    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enougth cash
        if order.status in [order.Completed]:
            if order.isbuy():
                self.log(
                    'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                    (order.executed.price,
                     order.executed.value,
                     order.executed.comm))

                self.buyprice = order.executed.price
                self.buycomm = order.executed.comm
                #self.log(self.position.size)
                

                
            else:  # Sell
                self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                         (order.executed.price,
                          order.executed.value,
                          order.executed.comm))
                #self.log(self.position.size)


            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        # Write down: no pending order
        self.order = None
        
    def notify_trade(self, trade):
        if not trade.isclosed:
            return

        self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f, POSITION OPEN %d' %
                 (trade.pnl, trade.pnlcomm,self.position.size))
        Trad_roe = trade.pnlcomm/trade.price*100
        trad_len = trade.barlen
        self.log('ROE - %.2f ' % Trad_roe)
        self.log(' ')
        self.trad_roe += Trad_roe
        self.t_tl += trad_len
#        data_trades = self._trades[data][0]
#        print(data_trades[self.io])
#        self.io += 1

    def next(self):
        
        # Simply log the closing price of the series from the reference
        #self.log("Close - %.2f | LE - %s | SE - %s " % (self.dataclose[0], self.last_long_entry, self.last_short_entry))
       # self.log('%.2f' % self.hull1.lines.hma[-1])
        # Check if an order is pending ... if yes, we cannot send a 2nd one
        if self.order:
            return
       
        self.t_c += 1
        
        # Stoploss Trigger Conditions
        long_sl = self.position.size > 0 and self.dataclose[0] < self.last_long_entry*((100 - self.slp)/100)
        short_sl = self.position.size < 0 and self.dataclose[0] > self.last_short_entry*((100 + self.slp)/100)
        
        # Long Trigger Conditions
        long = (((self.dataclose[-1] - self.dataopen[0]) < -self.diff ) or self.il) and self.long_positions == 0
        closelong = False and self.long_positions > 0 and (not long_sl )

        # Short Trigger Conditions
        short = ((self.dataclose[-1] - self.dataopen[0] > self.diff )) and self.short_positions == 0
        closeshort = False and self.short_positions <= -1 and (not short_sl )
        
        short_tp = self.position.size == -1 and self.dataclose[0] < self.last_short_entry*((100 - self.stp)/100)
        long_tp = self.position.size == 1 and self.dataclose[0] > self.last_long_entry*((100 + self.ltp)/100)


        if long_tp:
            self.log('LONG TP, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
            self.long_positions = 0
#            self.elsl_counter += 1
            self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100 
            self.last_long_entry = 0
#            self.il = True

            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()
        
        elif short_tp:
            self.log('SHORT TP, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
            self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100
            self.last_short_entry = 0
#            self.essl_counter += 1
            self.short_positions = 0
#            self.il = True

            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()  
        # Stoploss Trigger
        if long_sl:
            self.log('LONG STOP, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
            self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100 
#            self.last_long_entry = 0
            self.long_positions = 0
#            self.lsl_counter += 1
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()
        
        elif short_sl:
            self.log('SHORT STOP, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
            self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100
#            self.last_short_entry = 0
#            self.ssl_counter += 1
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()            
        
        # Check if we are in the market
        if self.position.size == 0:

            if long:

                self.log('LONG, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                
                # Keep track of the created order to avoid a 2nd order
                self.order = self.buy()
                self.long_positions = 1
                # Record Entry Position
                self.last_long_entry = self.dataclose[0]
                self.long_counter += 1
                #self.log(self.last_long_entry)
                self.il = False


            elif short:

                self.log('SHORT, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                
                # Keep track of the created order to avoid a 2nd order
                self.order = self.sell()                   
                self.short_counter += 1
                self.short_positions = -1
                self.last_short_entry = self.dataclose[0]
                #self.log(self.last_short_entry)

        else:

            if closelong:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('LONG CLOSE, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100              
                self.last_long_entry = 0
                self.long_positions = 0
                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()

            elif closeshort:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('CLOSE SHORT, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100 
                self.last_short_entry = 0
                self.short_positions = 0
                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()

val = 0
lol = 0
t_el = 0
t_es = 0
#montha = [4,5,6,7,8,9,10,11,12]
montha = [7]
for i in montha:
        
    if __name__ == '__main__':
        # Create a cerebro entity
        cerebro = bt.Cerebro()
    
        # Add a strategy
        cerebro.addstrategy(BTCUSD_5)
       
        # Datas are in a subfolder of the samples. Need to find where the script is
        # because it could have been called from anywhere
        modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
        datapath = os.path.join(modpath, 'NIFTY-17-5M.csv')
#        datapath2 = os.path.join(modpath, 'data\BTCUSD-17-6H.csv')
    
        # Create a Data Feed
        data = btfeeds.GenericCSVData(
        dataname= datapath,
        fromdate=datetime.datetime(2017, 1, 1),
        todate=datetime.datetime(2017, 12, 30),
    
        nullvalue=0,
    
        dtformat=('%d/%m/%Y %H:%M'),
        timeframe=bt.TimeFrame.Minutes, 
        compression=5,
    
    
        date=-1,
        datetime=0,
        high=2,
        low=3,
        open=1,
        close=4,
        volume=-1,
        openinterest=-1
        )
    
        # Add the Data Feed to Cerebro
        cerebro.adddata(data)
#        cerebro.adddata(data1)
        
        def printSQN(analyzer):
            sqn = round(analyzer.sqn,2)
            print('SQN: {}'.format(sqn))
    
        def printMDD(analyzer):
            mdd = round(analyzer.max.moneydown,2)
            print('MDD: {}'.format(mdd))
        
    
        # Set our desired cash start
        cerebro.broker.setcash(100000.0)     
    
        # Add a FixedSize sizer according to the stake
        cerebro.addsizer(bt.sizers.FixedSize, stake=1)
    
        # Set the commission - 0.1% ... divide by 100 to remove the %
        cerebro.broker.setcommission(commission=0.000)
 
    
        name = os.path.splitext(os.path.basename(__file__))[0]
        
        writeFile = open("/"+str(name)+".csv", "w")
        cerebro.addwriter(bt.WriterFile, out = writeFile, csv=True, rounding=2)
        
        # Set the Slippage
        cerebro.broker.set_slippage_fixed(0)
        print('Results for Month - %d' % i)
        # Print out the starting conditions
        print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())
        
        # Analyzer
        cerebro.addanalyzer(bt.analyzers.TradeAnalyzer, _name="ta")
        cerebro.addanalyzer(bt.analyzers.SQN, _name="sqn")
        cerebro.addanalyzer(bt.analyzers.DrawDown, _name="drawdown")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, _name="a_ol")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, filter='short',_name="s_ol")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, filter='long',_name="l_ol")
        
        # Run over everything
        strategies = cerebro.run()
        firstStrat = strategies[0]
        
        # Print out the final result
        print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())

        val += cerebro.broker.getvalue()

        tol = 0
        tol = (firstStrat.long_counter + firstStrat.short_counter ) * 0.4

        # print the analyzers 
        firstStrat.analyzers.a_ol.print()
        firstStrat.analyzers.l_ol.print()
        firstStrat.analyzers.s_ol.print()
        
        printSQN(firstStrat.analyzers.sqn.get_analysis())

        print('')
        
        printMDD(firstStrat.analyzers.drawdown.get_analysis())

    
        print("Long Stop Hits - {} | Short Stop Hits - {}" .format(firstStrat.long_stop_counter, firstStrat.short_stop_counter))
        
        print("SL - {}, Long TP - {}, Short TP -{}" .format(firstStrat.slp, firstStrat.ltp, firstStrat.stp))
        
        print('')
        
        print('ROE - %.2f | Net- %.2f ' % ((firstStrat.trad_roe), val-100000))
        lol += firstStrat.trad_roe
        print("Total Bars - %d, Trade Bars - %d, Time Open  - %s" % ((firstStrat.t_c), firstStrat.t_tl, (str(int(firstStrat.t_tl/firstStrat.t_c*100)) + '%') ))

        print(' ')
        
        cerebro.plot(style='candlesticks')
#print('ROE - %.2f' % lol)
writeFile.close()

import pandas as pd


df = pd.read_csv( '/'+str(name)+".csv", skiprows=2, parse_dates = [0] , dayfirst=True, 
                     usecols=[ 3 ,16 ,25])

df.columns = ['Date','value','ROE']

df = df.dropna(subset=['value'])

pd.to_datetime(df['Date'], format='%Y/%m/%d %H:%M')

df.index = pd.to_datetime(df['Date'])
df.drop(['Date'], axis = 1)
ohlc_dict = {'value':'last', 'ROE':'sum'}

df = df.resample('1W', how=ohlc_dict)
cols=['value', 'ROE']  
df = df[cols]
df = df.T
df.to_csv('N-GAP-W.csv')

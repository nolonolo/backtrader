import pandas as pd
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])
import numpy as np


modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
datapath = os.path.join(modpath, "E:/Anaconda3/Lib/site-packages/backtrader/strategies/data/NBatch/lol.csv")

df = pd.read_csv(datapath,parse_dates = [0], dayfirst = True, na_values=[np.nan], header=None,
                usecols=[0,1,2,3,4], index_col = 0)

df.columns = ['open','high','low','close']
#df.index = pd.to_datetime('13000101', format='%Y%m%d', errors='ignore')

ohlc_dict = {'open':'first', 'high':'max', 'low':'min', 'close': 'last'}

# Resample to 5Min (this format is needed) as per ohlc_dict, then remove any line with a NaN
df = df.resample('5Min', how=ohlc_dict)

# Resample mixes the columns so lets re-arrange them 
cols=['open', 'high', 'low', 'close']  
df = df[cols]

# Take rows with finite OHLC vaule ( Avoids NaN values)
df = df[np.isfinite(df['open'])]

# Write out to CSV
df.to_csv('NIFTY-14-5M.csv')
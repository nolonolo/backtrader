import pandas as pd
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])
import numpy as np

lista = ['JAN','FEB','MARCH','APRIL','MAY','JUNE','JULY','AUG','SEP','OCT','NOV','DEC']
#lista = ['JAN']
df2 = pd.DataFrame()
l = 1
for i in lista:
    
    modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
    datapath = os.path.join(modpath, "H://AlgoTrad/Backtesting/NIFTY/1M/2013/2013 " + str(i) + ' NIFTY.txt')
    
    df = pd.read_csv(datapath, sep=',',parse_dates = { 1 : [1, 2]}, usecols=[0 ,1, 2, 3, 4, 5, 6], skiprows = 0)

#    df = pd.read_csv(datapath, sep=',' , parse_dates = { 'bt' : [1, 2]}, dayfirst = False, na_values=[np.nan],
#                 usecols=[1, 2, 3, 4, 5, 6], index_col = 'bt')

    # Created a dictionary to tell Pandas how to re-sample, if this isn't in place it will re-sample each column separately 

#    ohlc_dict = {3:'first', 4:'max', 5:'min', 6: 'last'}
#
#    # Resample to 5Min (this format is needed) as per ohlc_dict, then remove any line with a NaN
#    df = df.resample('5Min', how=ohlc_dict)
#    #df.replace(r'/s+', np.nan, regex=True)
#    # Resample mixes the columns so lets re-arrange them 
#    cols=['open', 'high', 'low', 'close']  
#    df = df[cols]
    
    # Take rows with finite OLHC vaule ( Avoids NaN values)
#    df = df[np.isfinite(df['open'])]
    
#    df2 = df2.append(df)
    
    # Write out to CSV
#    df.to_csv('data/NBatch/NIFTY-' + str(i) + '-5M.csv')

    datapath2 = os.path.join(modpath, "E:/Anaconda3/Lib/site-packages/backtrader/strategies/data/NBatch/" + str(l) + '.csv')
    df.to_csv(datapath2)
    l += 1
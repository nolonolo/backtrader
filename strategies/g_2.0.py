from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import datetime  # For datetime objects
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])
import warnings
warnings.filterwarnings("ignore")

# Import the backtrader platform
import backtrader as bt
import backtrader.feeds as btfeeds
import backtrader.indicators as btind
from backtrader.indicators import hullrb
from backtrader.indicators import hma1
import backtrader.analyzers as btanalyzers
from backtrader.analyzers import basictradestats
# Create a Stratey
class BTCUSD_5(bt.Strategy):
    
    params = (
        ('maperiod', 550),
    )

    
    def log(self, txt, dt=None):
        #''' Logging function for this strategy'''
        dt = dt or self.datas[0].datetime.datetime(0)
#        print('%s' % (txt))

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.dataclose = self.datas[0].close

        # To keep track of pending orders and buy price/commission
        self.order = None
        self.buyprice = None
        self.buycomm = None

        # Add a MovingAverageSimple indicator
        self.sma = btind.SimpleMovingAverage(
            self.datas[0], period=self.params.maperiod)
        self.sma.plotinfo.plot = False
        
        self.hull_en = hullrb.HullMovingRB(self.datas[0], period=250, perc = 6)
  #      self.hull_en.plotinfo.plot = False
        self.hull = bt.indicators.HullMovingAverage(self.datas[0], period=550)
 #       self.hull.plotinfo.plot = False

        self.hull1 = bt.indicators.HullMovingAverage(self.datas[0], period=250)
        self.hull1.plotinfo.plot = True
        
        self.atr = bt.indicators.ATR(self.datas[0])
#        self.atr.plotlines.atr2._plotskip = True
#        self.atr.plotlines.atr._plotskip= True
        
        self.stoch = bt.indicators.StochasticFull(self.datas[0], period=50, period_dslow = 4)
   #     self.stoch.plotinfo.plot = True
        self.ssma = bt.indicators.SmoothedMovingAverageEnvelope(self.datas[0], period=250, perc = 0)
     #   self.ssma.plotinfo.plot = False
#        self.ssma1 = bt.indicators.SmoothedMovingAverage(self.datas[1], period=30)



        self.LOverBought = 85
        self.LOverSold = 10
        self.SOverBought = 90
        self.SOverSold = 10
        
        self.slp1 = 0.25
        self.slp2 = 0.25
        self.slp4 = 0.25
        
        self.avg1 = 10
        self.atr_trad = 0
        
        self.last_long_entry = 0
        self.last_short_entry = 0
        self.last_elong_entry = 0
        self.long_counter = 0
        self.short_counter = 0
        self.elong_counter = 0
        self.short_avg_counter = 0
        self.long_avg_counter = 0
        self.e_long_on = False
        self.trad_roe = 0
        
        self.stp = 0
        self.ltp = 0
        self.short_stop_counter = 0
        self.long_stop_counter = 0     
        self.OB_stoch_count = 0
        self.OS_stoch_count = 0
        
        # Custom Position Counting
        self.elong_positions = 0
        self.long_positions = 0
        self.short_positions = 0
        
    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enougth cash
        if order.status in [order.Completed]:
#            if order.isbuy():
#                self.log(
#                    'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
#                    (order.executed.price,
#                     order.executed.value,
#                     order.executed.comm))
#
#                self.buyprice = order.executed.price
#                self.buycomm = order.executed.comm
#                #self.log(self.position.size)
#                
#
#                
#            else:  # Sell
#                self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
#                         (order.executed.price,
#                          order.executed.value,
#                          order.executed.comm))
#                #self.log(self.position.size)


            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        # Write down: no pending order
        self.order = None
        
    def notify_trade(self, trade):
        if not trade.isclosed:
            return

        self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f, POSITION OPEN %d' %
                 (trade.pnl, trade.pnlcomm,self.position.size))
        self.log(' ')
        
    def next(self):
        
        # Simply log the closing price of the series from the reference
        #self.log("Close - %.2f | LE - %s | SE - %s " % (self.dataclose[0], self.last_long_entry, self.last_short_entry))
       # self.log('%.2f' % self.hull1.lines.hma[-1])
        # Check if an order is pending ... if yes, we cannot send a 2nd one
        if self.order:
            return
       
        
        # HullMA Trigger Calculation
        hull_tradable = True
        if self.hull1.lines.hma[0] > self.hull1.lines.hma[-1]:
            hull_tradable = True
        else:
            hull_tradable = False
       

        # ATR Trigger Calculation
        atr_tradable = True
        if self.atr.lines.atr3[0] > self.atr_trad:
            atr_tradable = True
        else:
            atr_tradable = False
            
        # Adpative Stoploss Trigger Conditions
        Short_adp_sl = self.dataclose[0] > self.hull_en.lines.top[0] and self.short_positions <= -1
        Long_adp_sl = self.hull.lines.hma[0] > self.hull_en.lines.hma[0] and self.long_positions >= 1
        
        # Stoploss Trigger Conditions
        long_sl = self.position.size > 0 and self.dataclose[0] < self.last_long_entry*((100 - self.slp1)/100)
        short_sl = self.position.size < 0 and self.dataclose[0] > self.last_short_entry*((100 + self.slp1)/100)
        
#        # Avg Trigger Conditions
#        long_avg = self.long_positions == 1 and self.dataclose[0] < self.last_long_entry*((100 - self.avg1)/100) and self.e_long_on == False and (not long_sl )
#        short_avg = self.short_positions == -1 and self.dataclose[0] > self.last_short_entry*((100 + self.avg1)/100) and (not short_sl )
        
        # Long Trigger Conditions
        long = (self.stoch.lines.percDSlow[0] < self.LOverSold ) and self.dataclose[0] > self.ssma.lines.bot[0] and hull_tradable and atr_tradable and self.long_positions == 0
        closelong = self.stoch.lines.percDSlow[0] > self.LOverBought and self.long_positions > 0 and (not long_sl )
        
        # Elong Trigger Conditions
        e_long = self.dataclose[0] < self.hull_en.lines.bot[0] and self.elong_positions == 0
        e_long_close =  (self.stoch.lines.percDSlow[0] > 87.0 ) and self.elong_positions == 1 

        # Short Trigger Conditions
        short = (self.stoch.lines.percDSlow[0] > self.SOverBought ) and self.dataclose[0] < self.ssma.lines.top[0] and hull_tradable and atr_tradable and self.short_positions == 0
        closeshort = (self.stoch.lines.percDSlow[0] < self.SOverSold) and self.short_positions < 0 and (not short_sl )
        
        # Reassign Stoplosses
        self.slp1 = self.slp2
        if Short_adp_sl:
            self.slp1 = self.slp4
        else:
            self.slp1 = self.slp1
        
        if Long_adp_sl:
            self.slp1 = self.slp4
        else:
            self.slp1 = self.slp1        
        
        # Stoch counter
        if self.stoch.lines.percDSlow[0] > self.LOverBought:
            self.OB_stoch_count += 1
        elif self.stoch.lines.percDSlow[0] < self.SOverSold:
            self.OS_stoch_count += 1
        
      
#        if long_avg:
#            self.log('LONG AVG, %.2f' % self.dataclose[0])
#            # Keep track of the created order to avoid a 2nd order
#            self.order = self.buy()
#            self.long_avg_counter += 1
#            self.long_positions += 1
#            
#        
#        elif short_avg:
#            self.log('SHORT AVG, %.2f' % self.dataclose[0])
#            # Keep track of the created order to avoid a 2nd order
#            self.order = self.sell() 
#            self.short_avg_counter += 1
#            self.short_positions -= 1

        # Stoploss Trigger
        if long_sl:
            self.log('LONG STOP, %.2f' % self.dataclose[0])

            if self.long_positions == 2:
                self.trad_roe += (-(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100)*1.5
            else:
                 self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100
            
            self.long_stop_counter += 1
            
            self.last_long_entry = 0
            self.long_positions = 0                
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()
        
        elif short_sl:
            self.log('SHORT STOP, %.2f' % self.dataclose[0])

            self.short_stop_counter += 1


            if self.short_positions == -2:
                self.trad_roe += ((self.last_short_entry - self.dataclose[0])/self.last_short_entry*100)*1.5
            else:
                self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100          
            
            self.short_positions = 0
            self.last_short_entry = 0
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()            
        
        # Check if we are in the market
        if self.position.size == 0:

            # Not yet ... we MIGHT BUY if ...
            if e_long:

                # BUY, BUY, BUY!!! (with all possible default parameters)
                self.log('E_LONG, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                self.elong_counter += 1
                self.e_long_on = True
                self.elong_positions = 1
                self.last_elong_entry = self.dataclose[0]
                # Keep track of the created order to avoid a 2nd order
                self.order = self.buy()
                
            
            elif long:

                self.log('LONG, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                
                # Keep track of the created order to avoid a 2nd order
                self.order = self.buy()
                self.long_positions = 1
                # Record Entry Position
                self.last_long_entry = self.dataclose[0]
                self.long_counter += 1
                #self.log(self.last_long_entry)                


            elif short:

                self.log('SHORT, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                
                # Keep track of the created order to avoid a 2nd order
                self.order = self.sell()                   
                self.short_counter += 1
                self.short_positions = -1
                self.last_short_entry = self.dataclose[0]
                #self.log(self.last_short_entry)

        else:

            if  e_long_close:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('E_LONG CLOSE, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                self.e_long_on = False
                self.elong_positions = 0
                self.trad_roe += -(self.last_elong_entry - self.dataclose[0])/self.last_elong_entry*100
                self.last_elong_entry = 0
                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()
                
            elif closelong:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('LONG CLOSE, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                if self.long_positions == 2:
                    self.trad_roe += (-(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100)*1.5
                else:
                    self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100
                
                self.long_positions = 0      
                self.last_long_entry = 0
                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()

            elif closeshort:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('CLOSE SHORT, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))

                if self.short_positions == -2:
                    self.trad_roe += ((self.last_short_entry - self.dataclose[0])/self.last_short_entry*100)*1.5
                else:
                    self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100          
                self.short_positions = 0
                self.last_short_entry = 0
                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()

val = 0
lol = 0
t_el = 0
t_es = 0
#lista = ['09','10','11','12','13','14','15','16','17']
lista = ['17']
bista = lista

         
for i in bista :
    
    if __name__ == '__main__':
        # Create a cerebro entity
        cerebro = bt.Cerebro()
    
    #    strats = cerebro.optstrategy(
    #        BTCUSD_5,
    #        maperiod=range(10, 31))
        
        # Add a strategy
#        cerebro.addstrategy(BTCUSD_5L)
    #    cerebro.addstrategy(BTCUSD_5S)
        cerebro.addstrategy(BTCUSD_5)
    
        # Datas are in a subfolder of the samples. Need to find where the script is
        # because it could have been called from anywhere
        modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
        datapath = os.path.join(modpath, 'data\XBatch\XAUUSD-'+i+'-5M.csv')
#        datapath2 = os.path.join(modpath, 'data\XBatch\XAUUSD-'+i+'-6H.csv')
    
        # Create a Data Feed
        data = btfeeds.GenericCSVData(
        dataname= datapath,
        fromdate=datetime.datetime(int('20'+i), 1, 1),
        todate=datetime.datetime(int('20'+i), 12, 31),
    
        nullvalue=0,
    
        dtformat=('%d/%m/%Y %H:%M'),
        timeframe=bt.TimeFrame.Minutes, 
        compression=5,
    
    
        date=-1,
        datetime=0,
        high=2,
        low=3,
        open=1,
        close=4,
        volume=-1,
        openinterest=-1
        )
    

        cerebro.adddata(data)

        
       
        def printSQN(analyzer):
            sqn = round(analyzer.sqn,2)
            print('SQN: {}'.format(sqn))
    
        def printMDD(analyzer):
            mdd = round(analyzer.max.moneydown,2)
            print('MDD: {}'.format(mdd))
        
            
        # Set our desired cash start
        cerebro.broker.setcash(100000.0)     
    
        # Add a FixedSize sizer according to the stake
        cerebro.addsizer(bt.sizers.FixedSize, stake=1)
    
        # Set the commission - 0.1% ... divide by 100 to remove the %
        cerebro.broker.setcommission(commission=0.00)
    
        # Set the Slippage
    #    cerebro.broker.set_slippage_fixed(0)
    
      #  bt.observers.Trades.plotlines.pnlplus.color = 'blue'
     #   bt.observers.Trades.plotlines.pnlminus.marker = 'o'
        print('Results for Year - %d' % int(i))
        # Print out the starting conditions
        print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())
        
        # Analyzer
    
        cerebro.addanalyzer(bt.analyzers.SQN, _name="sqn")
        cerebro.addanalyzer(bt.analyzers.DrawDown, _name="drawdown")
        cerebro.addanalyzer(bt.analyzers.Transactions, _name="transactions")
        
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, _name="a_ol")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, filter='short',_name="s_ol")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, filter='long',_name="l_ol")
        
        # Run over everything
        strategies = cerebro.run()
        firstStrat = strategies[0]
        

        # Print out the final result
        print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())
        
        val += cerebro.broker.getvalue()
        t_el += firstStrat.elong_counter
       
        tol = 0
        tol = (firstStrat.long_counter + firstStrat.short_counter + firstStrat.elong_counter) * 0.4
        # print the analyzers 
        firstStrat.analyzers.a_ol.print()
        firstStrat.analyzers.l_ol.print()
        firstStrat.analyzers.s_ol.print()
        
        printSQN(firstStrat.analyzers.sqn.get_analysis())
#        print(firstStrat._trades)
        print('')
        
        printMDD(firstStrat.analyzers.drawdown.get_analysis())
    #    print(firstStrat.analyzers.transactions.get_analysis())
    
        print("Long Stop Hits - {} | Short Stop Hits - {} | Stoch Hit {} - {} Times & Hit {} - {} Times" 
              .format(firstStrat.long_stop_counter, firstStrat.short_stop_counter,
                      firstStrat.LOverBought, firstStrat.OB_stoch_count,
                      firstStrat.SOverSold, firstStrat.OS_stoch_count,))
        
        print("SL - {}, Long TP - {}, Short TP -{}, Avg - {}, ATR - {}"
          .format(firstStrat.slp1, firstStrat.ltp,
                  firstStrat.stp,  firstStrat.avg1, firstStrat.atr_trad))
        print('')
        
        print('ROE - %.2f | Net- %.2f | Total trades - %d' % ((firstStrat.trad_roe), int(cerebro.broker.getvalue()), int(t_el)))
        lol += firstStrat.trad_roe
        
#        print('Total Avgs - %d, Long Avgs - %d' % ((firstStrat.long_avg_counter), firstStrat.long_avg_counter))
    #    firstStrat._trades[data][0].print()
#        print(round(val-100000*(i-3),2))
        print(' ')
        
        cerebro.plot(style='candlesticks')
print('ROE - %.2f' % lol)
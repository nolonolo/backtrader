from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import datetime  # For datetime objects
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])
import warnings
warnings.filterwarnings("ignore")

# Import the backtrader platform
import backtrader as bt
import backtrader.feeds as btfeeds
import backtrader.indicators as btind
from backtrader.indicators import hullrb
from backtrader.indicators import hma1
import backtrader.analyzers as btanalyzers
from backtrader.analyzers import basictradestats
# Create a Stratey
class BTCUSD_5(bt.Strategy):
    
    params = (
        ('maperiod', 550),
    )

    
    def log(self, txt, dt=None):
        #''' Logging function for this strategy'''
        dt = dt or self.datas[0].datetime.datetime(0)
        #print('%s' % (txt))

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.dataclose = self.datas[0].close

        # To keep track of pending orders and buy price/commission
        self.order = None
        self.buyprice = None
        self.buycomm = None

    # INdicators
        self.ssma1 = bt.indicators.SmoothedMovingAverageEnvelope(self.datas[0], period=500, perc = 0.5)
        self.hull1 = bt.indicators.HullMovingAverage(self.datas[0], period=250)
        self.hull2 = bt.indicators.HullMovingAverage(self.datas[0], period=150)

        # Inputs
        self.slp1 = 7.5
        self.slp2 = 5
        self.slp4 = 5
        self.atr_trad = 0.45

        self.last_long_entry = 0
        self.last_short_entry = 0
        self.trad_roe = 0

        self.long_counter = 0
        self.short_counter = 0
        self.elong_counter = 0
        self.short_avg_counter = 0
        self.long_avg_counter = 0
        
        # Custom Position Counting
        self.long_positions = 0

        self.short_positions = 0
        
    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enougth cash
        if order.status in [order.Completed]:
            if order.isbuy():
                self.log(
                    'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                    (order.executed.price,
                     order.executed.value,
                     order.executed.comm))

                self.buyprice = order.executed.price
                self.buycomm = order.executed.comm
                #self.log(self.position.size)
                

                
            else:  # Sell
                self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                         (order.executed.price,
                          order.executed.value,
                          order.executed.comm))
                #self.log(self.position.size)


            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        # Write down: no pending order
        self.order = None
        
    def notify_trade(self, trade):
        if not trade.isclosed:
            return

        self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f, POSITION OPEN %d' %
                 (trade.pnl, trade.pnlcomm,self.position.size))

    def next(self):
        
        # Simply log the closing price of the series from the reference
        #self.log("Close - %.2f | LE - %s | SE - %s " % (self.dataclose[0], self.last_long_entry, self.last_short_entry))
       # self.log('%.2f' % self.hull1.lines.hma[-1])
        # Check if an order is pending ... if yes, we cannot send a 2nd one
        if self.order:
            return
       
        # HullMA Trigger Calculation
        hull_tradable = True
        if self.hull1.lines.hma[0] > self.hull1.lines.hma[-1]:
            hull_tradable = True
        else:
            hull_tradable = False         
            
        hull_tradable_1 = True
        if self.hull2.lines.hma[0] > self.hull2.lines.hma[-1]:
            hull_tradable_1 = True
        else:
            hull_tradable_1 = False 
        
        # Long Conditions
        long = self.dataclose[0] > self.ssma1.lines.top[0] and hull_tradable and self.position.size == 10
#        close_long = self.dataclose[0] < self.ssma1.lines.smma[0]
        close_long =  self.dataclose[0] < self.ssma1.lines.bot[0] and (not hull_tradable_1) and self.position.size == 10
        
        # Long Conditions
        short = self.dataclose[0] < self.ssma1.lines.bot[0] and (not hull_tradable ) and self.position.size == 0
#        close_long = self.dataclose[0] < self.ssma1.lines.smma[0]
        close_short =  self.dataclose[0] > self.ssma1.lines.top[0] and hull_tradable_1 and self.position.size == -1
        
#        # Stoploss Trigger
#        if long_sl:
#            self.log('LONG STOP, %.2f' % self.dataclose[0])
#            self.last_long_entry = 0
#            self.elong_positions = 0
#            # Keep track of the created order to avoid a 2nd order
#            self.order = self.close()
#        
#        elif short_sl:
#            self.log('SHORT STOP, %.2f' % self.dataclose[0])
#            self.last_short_entry = 0
#            # Keep track of the created order to avoid a 2nd order
#            self.order = self.close()            
#        
        # Check if we are in the market
        if self.position.size == 0:
            
            if long:

                self.log('LONG, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                
                # Keep track of the created order to avoid a 2nd order
                self.order = self.buy()
                self.long_positions = 1
                # Record Entry Position
                self.last_long_entry = self.dataclose[0]
                self.long_counter += 1
                #self.log(self.last_long_entry)                


            elif short:

                self.log('SHORT, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                
                # Keep track of the created order to avoid a 2nd order
                self.order = self.sell()                   
                self.short_counter += 1
                self.short_positions = -1
                self.last_short_entry = self.dataclose[0]
                #self.log(self.last_short_entry)

        else:

            if close_long:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('LONG CLOSE, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100 
                self.last_long_entry = 0
                self.long_positions = 0
                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()

            elif close_short:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('CLOSE SHORT, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100 
                self.last_short_entry = 0
                self.short_positions = 0
                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()

val = 0
lol = 0
t_el = 0
montha = [4,5,6,7,8,9,10,11,12]
#montha = [12]
for i in montha:
    if __name__ == '__main__':
        # Create a cerebro entity
        cerebro = bt.Cerebro()
    
        # Add a strategy
        cerebro.addstrategy(BTCUSD_5)
        
        # Datas are in a subfolder of the samples. Need to find where the script is
        # because it could have been called from anywhere
        modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
        datapath = os.path.join(modpath, 'data\BTCUSD-17-5M.csv')
#        datapath1 = os.path.join(modpath, 'BTCUSD-17-1M.csv')
        
        # Create a Data Feed
        data = btfeeds.GenericCSVData(
        dataname= datapath,
        fromdate=datetime.datetime(2017, 1, 1),
        todate=datetime.datetime(2017, i, 30),
    
        nullvalue=0.0,
    
        dtformat=('%d/%m/%Y %H:%M'),
        timeframe=bt.TimeFrame.Minutes, 
        compression=5,
    
    
        date=-1,
        datetime=0,
        high=2,
        low=3,
        open=1,
        close=4,
        volume=-1,
        openinterest=-1
        )
        
#        data1 = btfeeds.GenericCSVData(
#        dataname= datapath1,
#        fromdate=datetime.datetime(2017, i, 1),
#        todate=datetime.datetime(2017, i, 30),
#    
#        nullvalue=0.0,
#    
#        dtformat=('%d/%m/%Y %H:%M'),
#        timeframe=bt.TimeFrame.Minutes, 
#        compression=1,
#    
#    
#        date=-1,
#        datetime=0,
#        high=2,
#        low=3,
#        open=1,
#        close=4,
#        volume=-1,
#        openinterest=-1
#        )
        

        # Add the Data Feed to Cerebro
        cerebro.adddata(data)
        
#        cerebro.resampledata(data,timeframe=bt.TimeFrame.Minutes,
#                             compression=72)
#        cerebro.adddata(data1)
#        data1.plotinfo.plot = False
        
        def printTradeAnalysis(analyzer):
            avg_trade_time = analyzer.len.average * 5/60
            r2 = [' %.2f Hrs '% (avg_trade_time)]
            print(r2)
            
        def printSQN(analyzer):
            sqn = round(analyzer.sqn,2)
            print('SQN: {}'.format(sqn))
    
        def printMDD(analyzer):
            mdd = round(analyzer.max.moneydown,2)
            print('MDD: {}'.format(mdd))
        
    
        # Set our desired cash start
        cerebro.broker.setcash(100000.0)     
    
        # Add a FixedSize sizer according to the stake
        cerebro.addsizer(bt.sizers.FixedSize, stake=1)
    
        # Set the commission - 0.1% ... divide by 100 to remove the %
        cerebro.broker.setcommission(commission=0.002)
    
        # Set the Slippage
        cerebro.broker.set_slippage_fixed(0)
        print('Results for Month - %d' % i)
        # Print out the starting conditions
        print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())
        
        # Analyzer
        cerebro.addanalyzer(bt.analyzers.TradeAnalyzer, _name="ta")
        cerebro.addanalyzer(bt.analyzers.SQN, _name="sqn")
        cerebro.addanalyzer(bt.analyzers.DrawDown, _name="drawdown")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, _name="a_ol")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, filter='short',_name="s_ol")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, filter='long',_name="l_ol")
        
        # Run over everything
        strategies = cerebro.run()
        firstStrat = strategies[0]
        

        # Print out the final result
        print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())

        val += cerebro.broker.getvalue()
        t_el += firstStrat.elong_counter
        # print the analyzers 
        firstStrat.analyzers.a_ol.print()
#        firstStrat.analyzers.l_ol.print()
#        firstStrat.analyzers.s_ol.print()
        printSQN(firstStrat.analyzers.sqn.get_analysis())
#        print(firstStrat._trades)
        print('')
        printMDD(firstStrat.analyzers.drawdown.get_analysis())
    #    print(firstStrat.analyzers.transactions.get_analysis())
    
#        print("El Stop Hits - {} | ES Stop Hits - {} | Stoch Hit {} - {} Times & Hit {} - {} Times" 
#              .format(firstStrat.elsl_counter, firstStrat.essl_counter,
#                      firstStrat.OBstoch_value, firstStrat.OB_stoch_count,
#                      firstStrat.OSstoch_value, firstStrat.OS_stoch_count,))
#        print("SL - {}, Long TP - {}, Short TP - {}, MA1 - {}, MA2 - {}, Dev - {}" 
#          .format(firstStrat.slp1, firstStrat.ltp,
#                  firstStrat.stp, firstStrat.ma1,
#                  firstStrat.ma2, firstStrat.dev,))
        print('')
        print('ROE - %.2f | Net- %.2f | Total trades - %d' % (firstStrat.trad_roe, val-100000*(i-3), t_el))
#        lol += firstStrat.trad_roe
    #    print('Total Avgs - %d, Long Avgs - %d' % ((firstStrat.long_avg_counter), firstStrat.long_avg_counter))
    #    firstStrat._trades[data][0].print()
#        print(round(val-100000*(i-3),2))
        print(' ')
        
        cerebro.plot(style='candlesticks')
#print(round(val-900000,2))
#print(lol)
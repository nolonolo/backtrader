from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import datetime  # For datetime objects
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])

# Import the backtrader platform
import backtrader as bt
import backtrader.feeds as btfeeds
import backtrader.indicators as btind
from backtrader.indicators import hullrb
       
# Create a Stratey
class TestStrategy(bt.Strategy):
    
    def log(self, txt, dt=None):
        #''' Logging function for this strategy'''
        dt = dt or self.datas[0].datetime.datetime(0)
        print('%s, %s' % (dt.isoformat(), txt))

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.o = self.datas[0].open
        self.h = self.datas[0].high
        self.l = self.datas[0].low
        self.c = self.datas[0].close

        # To keep track of pending orders and buy price/commission
        self.order = None
        self.buyprice = None
        self.buycomm = None

        self.o_add = 0
        self.h_add = 0
        self.l_add = 0
        self.c_add = 0
        self.add_add = 0
        
        
    def next(self):
        
        # Simply log the closing price of the series from the reference
        self.log("Close - %.2f | OA - %.4f | HA - %.4f | LA - %.4f | CA - %.4f | AA - %.4f " % (self.c[0], self.o_add, self.h_add, self.l_add, self.c_add, self.add_add))
        
        self.o_add = (self.o_add + self.o[0])/1000
        self.h_add = (self.h_add + self.h[0])/1000
        self.l_add = (self.l_add + self.l[0])/1000
        self.c_add = (self.c_add + self.c[0])/1000
        self.add_add = (self.add_add + self.o_add + self.h_add + self.l_add + self.c_add)
        


if __name__ == '__main__':
    # Create a cerebro entity
    cerebro = bt.Cerebro()

    # Add a strategy
    cerebro.addstrategy(TestStrategy)
    
    # Datas are in a subfolder of the samples. Need to find where the script is
    # because it could have been called from anywhere
    modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
    datapath = os.path.join(modpath, 'XAUUSD-2-18-5M.csv')

    # Create a Data Feed
    data = btfeeds.GenericCSVData(
    dataname= datapath,
    fromdate=datetime.datetime(2018, 2, 1),
    todate=datetime.datetime(2018, 2, 15),

    

    dtformat=('%d/%m/%Y %H:%M'),
    timeframe=bt.TimeFrame.Minutes, 
    compression=1,


    date=-1,
    datetime=0,
    high=2,
    low=3,
    open=1,
    close=4,
    volume=-1,
    openinterest=-1
    )
    
        
    # Add the Data Feed to Cerebro
    cerebro.adddata(data)

    # Set our desired cash start
    cerebro.broker.setcash(100000.0)     

    # Add a FixedSize sizer according to the stake
    cerebro.addsizer(bt.sizers.FixedSize, stake=1)

    # Set the commission - 0.1% ... divide by 100 to remove the %
    cerebro.broker.setcommission(commission=0.0)

    # Print out the starting conditions
    print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())

    # Run over everything
    cerebro.run()

    # Print out the final result
    print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())
    
    cerebro.plot(style='candlestick', barup='green', bardown='red')

from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import datetime  # For datetime objects
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])

# Import the backtrader platform
import backtrader as bt
import backtrader.feeds as btfeeds
import backtrader.indicators as btind
from backtrader.indicators import hullrb
from backtrader.indicators import hma1
import backtrader.analyzers as btanalyzers
# Create a Stratey
class BTCUSD_5(bt.Strategy):
    
    params = (
        ('maperiod', 550),
    )

    
    def log(self, txt, dt=None):
        #''' Logging function for this strategy'''
        dt = dt or self.datas[0].datetime.datetime(0)
        print('%s' % (txt))

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.dataclose = self.datas[0].close

        # To keep track of pending orders and buy price/commission
        self.order = None
        self.buyprice = None
        self.buycomm = None

        # Add a MovingAverageSimple indicator
        self.sma = btind.SimpleMovingAverage(
            self.datas[0], period=self.params.maperiod)
        self.sma.plotinfo.plot = False
        
        self.hull_en = hullrb.HullMovingRB(self.datas[0], period=250, perc = 0.36)
  #      self.hull_en.plotinfo.plot = False
        self.hull = bt.indicators.HullMovingAverage(self.datas[0], period=550)
 #       self.hull.plotinfo.plot = False

        self.hull1 = bt.indicators.HullMovingAverage(self.datas[0], period=250)
        self.hull1.plotinfo.plot = True
        
        self.atr = bt.indicators.ATR(self.datas[0])
        self.atr.plotlines.atr2._plotskip = True
        self.atr.plotlines.atr._plotskip= True
        
        self.stoch = bt.indicators.StochasticFull(self.datas[0], period=24)
   #     self.stoch.plotinfo.plot = True
        self.ssma = bt.indicators.SmoothedMovingAverageEnvelope(self.datas[0], period=150, perc = 0.001)
     #   self.ssma.plotinfo.plot = False

        self.LOverBought = 90.0
        self.LOverSold = 0.0
        self.SOverBought = 900.0
        self.SOverSold = 15.0
        self.slp1 = 0.45
        self.slp2 = 0.45
        self.slp4 = 0.45
        self.avg1 = 0.2
        self.atr_trad = -0.0
        self.last_long_entry = 0
        self.last_short_entry = 0
        self.long_counter = 0
        self.short_counter = 0
        self.elong_counter = 0
        self.short_avg_counter = 0
        self.long_avg_counter = 0

        self.e_long_on = False
        # Custom Position Counting
        self.elong_positions = 0
        
        self.long_positions = 0

        self.short_positions = 0
        
    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enougth cash
        if order.status in [order.Completed]:
            if order.isbuy():
                self.log(
                    'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                    (order.executed.price,
                     order.executed.value,
                     order.executed.comm))

                self.buyprice = order.executed.price
                self.buycomm = order.executed.comm
                #self.log(self.position.size)
                

                
            else:  # Sell
                self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                         (order.executed.price,
                          order.executed.value,
                          order.executed.comm))
                #self.log(self.position.size)


            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        # Write down: no pending order
        self.order = None
        
    def notify_trade(self, trade):
        if not trade.isclosed:
            return

        self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f, POSITION OPEN %d' %
                 (trade.pnl, trade.pnlcomm,self.position.size))

    def next(self):
        
        # Simply log the closing price of the series from the reference
        #self.log("Close - %.2f | LE - %s | SE - %s " % (self.dataclose[0], self.last_long_entry, self.last_short_entry))
       # self.log('%.2f' % self.hull1.lines.hma[-1])
        # Check if an order is pending ... if yes, we cannot send a 2nd one
        if self.order:
            return
       
        
        # HullMA Trigger Calculation
        hull_tradable = True
        if self.hull1.lines.hma[0] > self.hull1.lines.hma[-1]:
            hull_tradable = True
        else:
            hull_tradable = False
       

        # ATR Trigger Calculation
        atr_tradable = True
        if self.atr.lines.atr3[0] > self.atr_trad:
            atr_tradable = True
        else:
            atr_tradable = False
            
        # Avg Trigger Conditions
        long_avg = self.long_positions == 1 and self.dataclose[0] < self.last_long_entry*((100 - self.avg1)/100) and self.e_long_on == False
        short_avg = self.short_positions == -1 and self.dataclose[0] > self.last_short_entry*((100 + self.avg1)/100)
        
        # Stoploss Trigger Conditions
        long_sl = self.position.size > 0 and self.dataclose[0] < self.last_long_entry*((100 - self.slp1)/100)
        short_sl = self.position.size < 0 and self.dataclose[0] > self.last_short_entry*((100 + self.slp1)/100)
        
        # Adpative Stoploss Trigger Conditions
        Short_adp_sl = self.dataclose[0] > self.hull_en.lines.top[0] and self.short_positions <= -1
        Long_adp_sl = self.hull.lines.hma[0] > self.hull_en.lines.hma[0] and self.long_positions >= 1
        
        # Long Trigger Conditions
        long = (self.stoch.lines.percDSlow[0] < self.LOverSold ) and self.dataclose[0] > self.ssma.lines.bot[0] and atr_tradable and hull_tradable and self.long_positions == 0
        closelong = self.stoch.lines.percDSlow[0] > self.LOverBought and self.long_positions > 0
        
        # Elong Trigger Conditions
        e_long = self.dataclose[0] < self.hull_en.lines.bot[0] and self.elong_positions == 0
        e_long_close =  (self.stoch.lines.percDSlow[0] > 87.0 ) and self.elong_positions == 1 

        # Short Trigger Conditions
        short = (self.stoch.lines.percDSlow[0] > self.SOverBought ) and self.dataclose[0] < self.ssma.lines.top[0] and atr_tradable and hull_tradable and self.short_positions == 0
        closeshort = (self.stoch.lines.percDSlow[0] < self.SOverSold) and self.short_positions <= -1 
        
        # Reassign Stoplosses
        self.slp1 = self.slp2
        if Short_adp_sl:
            self.slp1 = self.slp4
        else:
            self.slp1 = self.slp1
        
        if Long_adp_sl:
            self.slp1 = self.slp4
        else:
            self.slp1 = self.slp1        
        

        
        if long_avg:
            self.log('LONG AVG, %.2f' % self.dataclose[0])
            # Keep track of the created order to avoid a 2nd order
            self.order = self.buy()
            self.long_avg_counter += 1
            self.long_positions += 1
            
        
        elif short_avg:
            self.log('SHORT AVG, %.2f' % self.dataclose[0])
            # Keep track of the created order to avoid a 2nd order
            self.order = self.sell() 
            self.short_avg_counter += 1
            self.short_positions -= 1

        # Stoploss Trigger
        if long_sl:
            self.log('LONG STOP, %.2f' % self.dataclose[0])
            self.last_long_entry = 0
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()
        
        elif short_sl:
            self.log('SHORT STOP, %.2f' % self.dataclose[0])
            self.last_short_entry = 0
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()            
        
        # Check if we are in the market
        if self.position.size == 0:

            # Not yet ... we MIGHT BUY if ...
            if e_long:

                # BUY, BUY, BUY!!! (with all possible default parameters)
                self.log('E_LONG, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                self.elong_counter += 1
                self.e_long_on = True
                self.elong_positions = 1
                self.last_long_entry = self.dataclose[0]
                # Keep track of the created order to avoid a 2nd order
                self.order = self.buy()
                
            
            elif long:

                self.log('LONG, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                
                # Keep track of the created order to avoid a 2nd order
                self.order = self.buy()
                self.long_positions = 1
                # Record Entry Position
                self.last_long_entry = self.dataclose[0]
                self.long_counter += 1
                #self.log(self.last_long_entry)                


            elif short:

                self.log('SHORT, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                
                # Keep track of the created order to avoid a 2nd order
                self.order = self.sell()                   
                self.short_counter += 1
                self.short_positions = -1
                self.last_short_entry = self.dataclose[0]
                #self.log(self.last_short_entry)

        else:

            if  e_long_close:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('E_LONG CLOSE, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                self.e_long_on = False
                self.elong_positions = 0
                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()
                
            elif closelong:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('LONG CLOSE, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                
                self.last_long_entry = 0
                self.long_positions = 0
                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()

            elif closeshort:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('CLOSE SHORT, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                self.last_short_entry = 0
                self.short_positions = 0
                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()
                


if __name__ == '__main__':
    # Create a cerebro entity
    cerebro = bt.Cerebro()

    # Add a strategy
    cerebro.addstrategy(BTCUSD_5)
    
    # Datas are in a subfolder of the samples. Need to find where the script is
    # because it could have been called from anywhere
    modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
    datapath = os.path.join(modpath, 'XAUUSD-5M.csv')

    # Create a Data Feed
    data = btfeeds.GenericCSVData(
    dataname= datapath,
    fromdate=datetime.datetime(2018, 1, 1),
    todate=datetime.datetime(2018, 1, 30),

    nullvalue=0,

    dtformat=('%d/%m/%Y %H:%M'),
    timeframe=bt.TimeFrame.Minutes, 
    compression=1,


    date=-1,
    datetime=0,
    high=2,
    low=3,
    open=1,
    close=4,
    volume=-1,
    openinterest=-1
    )
    
        
    # Add the Data Feed to Cerebro
    cerebro.adddata(data)
    
    def printTradeAnalysis(analyzer):

        #Get the results we are interested in
        total_open = analyzer.total.open
        total_closed = analyzer.total.closed
        total_won = analyzer.won.total
        total_lost = analyzer.lost.total
        pnl_avg = analyzer.pnl.net.average
        lose_streak = analyzer.streak.lost.longest
        pnl_net = round(analyzer.pnl.net.total,2)
        strike_rate = (total_won / total_closed) * 100
 #       pf = analyzer.pnl.gross.total/(analyzer.pnl.gross.total-analyzer.pnl.net.total)
        avg_trade_time = analyzer.len.average * 5/60
        
        #Designate the rows
        h1 = ['Total Open', 'Total Closed', 'Total Won', 'Total Lost', 'PF']
        h2 = ['Strike Rate','Avg Win', 'Losing Streak', 'PnL Net', 'Avg Time']
        r1 = [total_open, total_closed,total_won,total_lost,0]
        r2 = [round(strike_rate,2), round(pnl_avg,2), lose_streak, pnl_net,' %.2f Hrs '% (avg_trade_time)]
        #Check which set of headers is the longest.
        if len(h1) > len(h2):
            header_length = len(h1)
        else:
            header_length = len(h2)
            #Print the rows
            print_list = [h1,r1,h2,r2]
            row_format ="{:<15}" * (header_length + 1)
            print("Trade Analysis Results:")
            for row in print_list:
                  print(row_format.format('',*row))
 
    
    def printSQN(analyzer):
        sqn = round(analyzer.sqn,2)
        print('SQN: {}'.format(sqn))

    def printMDD(analyzer):
        mdd = round(analyzer.max.moneydown,2)
        print('MDD: {}'.format(mdd))
    

    # Set our desired cash start
    cerebro.broker.setcash(100000.0)     

    # Add a FixedSize sizer according to the stake
    cerebro.addsizer(bt.sizers.FixedSize, stake=1)

    # Set the commission - 0.1% ... divide by 100 to remove the %
    cerebro.broker.setcommission(commission=0.00)

    # Set the Slippage
#    cerebro.broker.set_slippage_fixed(0)

    # Print out the starting conditions
    print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())
    
    # Analyzer
    cerebro.addanalyzer(bt.analyzers.TradeAnalyzer, _name="ta")
    cerebro.addanalyzer(bt.analyzers.SQN, _name="sqn")
    cerebro.addanalyzer(bt.analyzers.DrawDown, _name="drawdown")
    cerebro.addanalyzer(bt.analyzers.Transactions, _name="transactions")

    # Run over everything
    strategies = cerebro.run()
    firstStrat = strategies[0]
    
    # Print out the final result
    print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())
   
    
    # print the analyzers 
    printTradeAnalysis(firstStrat.analyzers.ta.get_analysis())
    printSQN(firstStrat.analyzers.sqn.get_analysis())
    printMDD(firstStrat.analyzers.drawdown.get_analysis())
#    print(firstStrat.analyzers.transactions.get_analysis())
    
    print("Longs - {aa}, Shorts - {bb}, Elongs - {cc}" .format(aa=firstStrat.long_counter, bb=firstStrat.short_counter, cc=firstStrat.elong_counter))
    print('Total Avgs - %d, Long Avgs - %d, Short Avgs - %d' % ((firstStrat.long_avg_counter+firstStrat.short_avg_counter), firstStrat.long_avg_counter,firstStrat.short_avg_counter ))
#    print(firstStrat._trades[data][0])


        
    #cerebro.plot(style='candlestick', barup='green', bardown='red')

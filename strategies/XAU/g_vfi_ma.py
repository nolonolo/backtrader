from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import datetime  # For datetime objects
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])
import warnings
warnings.filterwarnings("ignore")

# Import the backtrader platform
import backtrader as bt
import backtrader.feeds as btfeeds
from backtrader.indicators import kamastddev
from backtrader.indicators import kamakmdev
from backtrader.indicators import dmastddev
# Create a Stratey
class BTCUSD_5(bt.Strategy):
    
    def log(self, txt, dt=None):
        #''' Logging function for this strategy'''
        dt = dt or self.datas[0].datetime.datetime(0)
#        print('%s' % (txt))

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.dataclose = self.datas[0].close
        

        # To keep track of pending orders and buy price/commission
        self.order = None
        self.buyprice = None
        self.buycomm = None
        
        self.ma1 = 1500
        self.ma2 = 500
        self.dev = 2.5
        # Indicators
#        self.stoch = bt.indicators.StochasticFull(self.datas[0], period=14, period_dslow = 4, safediv=True)
#        self.hull1 = bt.indicators.HullMovingAverage(self.datas[1], period=250)
#        self.hull_d = kamastddev.Kamastddev(self.datas[0], period=30, dev_period = 30, devfactor = self.dev)

#        self.hull_d = kamakmdev.Kamakmdev(self.datas[0], period=40, dev_period = 40, devfactor = self.dev)
#        self.hull_d.plotinfo.plot = False

        self.hull_d = dmastddev.Dmastddev(self.datas[0], period=250, hperiod=250, dev_period = 30, devfactor = self.dev)

        self.ssma2 = bt.indicators.SmoothedMovingAverageEnvelope(self.datas[0], period=250)

#        self.atr = bt.indicators.ATR(self.datas[0])
#        self.atr.plotlines.atr2._plotskip = True
#        self.atr.plotlines.atr._plotskip= True        
#      
        # Variables
        self.slp1 = 3.75
        self.avg1 = 50
        self.stp = 10
        self.ltp = 200
        self.atr_trad = 10

        self.last_long_entry = 0
        self.last_short_entry = 0
        
        self.elong_counter = 0
        self.eshort_counter = 0
        
        self.long_avg_counter = 0
        
        self.elsl_counter = 0
        self.essl_counter = 0
        
        self.OB_stoch_count = 0
        self.OS_stoch_count = 0
        
        self.OBstoch_value = 99
        self.OSstoch_value = 4.5
        
        self.trad_roe = 0
        self.il = True
        self.t_el = 0
        
        # Custom Position Counting
        self.elong_positions = 0
        self.eshort_positions = 0
        
    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enougth cash
        if order.status in [order.Completed]:
            lolol = 3
#            if order.isbuy():
#                self.log(
#                    'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
#                    (order.executed.price,
#                     order.executed.value,
#                     order.executed.comm))
#
#                self.buyprice = order.executed.price
#                self.buycomm = order.executed.comm
#                self.log(self.position.size)
                
#            else:  # Sell
#                self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
#                         (order.executed.price,
#                          order.executed.value,
#                          order.executed.comm))
#                self.log(self.position.size)


#            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        # Write down: no pending order
        self.order = None
        
    def notify_trade(self, trade):
        if not trade.isclosed:
            return

        self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f, POSITION OPEN %d' %
                 (trade.pnl, trade.pnlcomm,self.position.size))

    def next(self):
        
        # Simply log the closing price of the series from the reference
        #self.log("Close - %.2f | LE - %s | SE - %s " % (self.dataclose[0], self.last_long_entry, self.last_short_entry))
       # self.log('%.2f' % self.hull1.lines.hma[-1])
        # Check if an order is pending ... if yes, we cannot send a 2nd one
        if self.order:
            return
       
        #Trade Cond
        shull_tradable = self.dataclose[0] < self.ssma2.lines.smma[0]
        lhull_tradable = self.dataclose[0] > self.ssma2.lines.smma[0]
#        shull_tradable = self.ssma.lines.hma[0] > self.ssma1.lines.hma[0]
#        lhull_tradable = self.ssma.lines.hma[0] < self.ssma1.lines.hma[0]

        # ATR Trigger Calculation
        atr_tradable = True
#        if self.atr.lines.atr3[0] < self.atr_trad:
#            atr_tradable = True
#        else:
#            atr_tradable = True
        
        # HullMA Trigger Calculation
#        lhull_tradable = True
#        shull_tradable = True
#        if self.hull1.lines.hma[0] > self.hull1.lines.hma[-5]:
#            hull_tradable = True
#        else:
#            hull_tradable = False
        # Avg Trigger Conditions
#        long_avg = self.elong_positions == 1 and self.dataclose[0] < self.last_long_entry*((100 - self.avg1)/100) 
#        short_avg = self.short_positions == -1 and self.dataclose[0] > self.last_short_entry*((100 + self.avg1)/100)
        
        # Stoploss Trigger Conditions
        long_sl = self.position.size == 1 and self.dataclose[0] < self.last_long_entry*((100 - self.slp1)/100)
        short_sl = self.position.size == -1 and self.dataclose[0] > self.last_short_entry*((100 + self.slp1)/100)

#         Take Profit Trigger Conditions
        short_tp = self.position.size == -1 and self.dataclose[0] < self.last_short_entry*((100 - self.stp)/100)
        long_tp = self.position.size == 1 and self.dataclose[0] > self.last_long_entry*((100 + self.ltp)/100)

        # Elong Trigger Conditions
        e_long = self.dataclose[0] < self.hull_d.lines.b_dev[0] and ( self.il or self.elong_positions == -1) and lhull_tradable and atr_tradable 
        e_short = self.dataclose[0] > self.hull_d.lines.t_dev[0] and ( self.il or self.elong_positions == 1) and shull_tradable and atr_tradable
#        
#        # Close Conditions
#        e_long_close =  (self.stoch.lines.percDSlow[0] > self.OBstoch_value ) and self.elong_positions == 1 and (not long_tp )
#        e_short_close =  (self.stoch.lines.percDSlow[0] < self.OSstoch_value ) and self.eshort_positions == -1 and (not short_tp )
#        
        
#        # Stoch counter
#        if self.stoch.lines.percDSlow[0] > self.OBstoch_value:
#            self.OB_stoch_count += 1
#        elif self.stoch.lines.percDSlow[0] < self.OSstoch_value:
#            self.OS_stoch_count += 1
#        

            
        # Stoploss Trigger
        if long_sl:
            self.log('LONG STOP, %.2f' % self.dataclose[0])
#            self.elong_positions = -1
            self.elsl_counter += 1
            self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100 
#            self.last_long_entry = 200000
            # Keep track of the created order to avoid a 2nd order
            self.il = True
            self.order = self.close()
        
        elif short_sl:
            self.log('SHORT STOP, %.2f' % self.dataclose[0])
            self.essl_counter += 1
            self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100
#            self.last_short_entry = 0
#            self.elong_positions = -1
            self.il = True
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()     
#
        # Takeprofit Trigger
        if long_tp:
            self.log('LONG TP, %.2f' % self.dataclose[0])
#            self.elong_positions = 0
#            self.elsl_counter += 1
            self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100 
#            self.last_long_entry = 200000
            self.il = True

            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()
        
        elif short_tp:
            self.log('SHORT TP, %.2f' % self.dataclose[0])
            self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100
#            self.last_short_entry = 0
#            self.essl_counter += 1
#            self.eshort_positions = 0
            self.il = True

            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()  
            
#        if long_avg:
#            self.log('E_LONG AVG, %.2f' % self.dataclose[0])
#            # Keep track of the created order to avoid a 2nd order
#            self.order = self.buy()
#            self.long_avg_counter += 1
#            self.elong_positions += 1
            
        
#        elif short_avg:
#            self.log('SHORT AVG, %.2f' % self.dataclose[0])
#            # Keep track of the created order to avoid a 2nd order
#            self.order = self.sell() 
#            self.short_avg_counter += 1
#            self.short_positions -= 1       
        
#        # Check if we are in the market
#        if self.position.size == 0:
        
        # Not yet ... we MIGHT BUY if ...
        if e_long:

            self.log('E_LONG, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
            self.elong_counter += 1
            self.elong_positions = 1
            self.last_long_entry = self.dataclose[0]
            if self.il:
                self.last_short_entry = self.dataclose[0]            
            self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100 
            self.order = self.close()
            # Keep track of the created order to avoid a 2nd order
            self.order = self.buy()
            self.il = False
        
        elif  e_short:
            
            self.log('E_SHORT, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
            self.eshort_counter += 1
            self.elong_positions = -1
            self.last_short_entry = self.dataclose[0]
            if self.il:
                self.last_long_entry = self.dataclose[0]
            self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100
            
            self.order = self.close()
            # Keep track of the created order to avoid a 2nd order
            self.order = self.sell()
            self.il = False
                
                
#        else:
#
#            if  e_long_close:
#                # SELL, SELL, SELL!!! (with all possible default parameters)
#                self.log('E_LONG CLOSE, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
#                self.elong_positions = 0
#                self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100 
##                self.last_long_entry = 200000
#                self.log(self.trad_roe)
#                # Keep track of the created order to avoid a 2nd order
#                self.order = self.close()
#            
#            elif  e_short_close:
#                # SELL, SELL, SELL!!! (with all possible default parameters)
#                self.log('E_SHORT CLOSE, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
#                self.eshort_positions = 0
#                self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100
##                self.last_short_entry = 0
#                # Keep track of the created order to avoid a 2nd order
#                self.order = self.close()

val = 0
lol = 0
t_el = 0
t_es = 0
#lista = ['09','10','11','12','13','14','15','16','17']
lista = ['17']
bista = lista

         
for i in bista :
    
    if __name__ == '__main__':
        # Create a cerebro entity
        cerebro = bt.Cerebro()
    
    #    strats = cerebro.optstrategy(
    #        BTCUSD_5,
    #        maperiod=range(10, 31))
        
        # Add a strategy
#        cerebro.addstrategy(BTCUSD_5L)
    #    cerebro.addstrategy(BTCUSD_5S)
        cerebro.addstrategy(BTCUSD_5)
    
        # Datas are in a subfolder of the samples. Need to find where the script is
        # because it could have been called from anywhere
        modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
        datapath = os.path.join(modpath, 'E:\\Anaconda3\\Lib\\site-packages\\backtrader\\strategies\\data\\XBatch\\XAUUSD-17-5M.csv')
#        datapath2 = os.path.join(modpath, 'data\XBatch\XAUUSD-'+i+'-6H.csv')
    
        # Create a Data Feed
        data = btfeeds.GenericCSVData(
        dataname= datapath,
        fromdate=datetime.datetime(int('20'+i), 1, 1),
        todate=datetime.datetime(int('20'+i), 12, 31),
    
        nullvalue=0,
    
        dtformat=('%d/%m/%Y %H:%M'),
        timeframe=bt.TimeFrame.Minutes, 
        compression=5,
    
    
        date=-1,
        datetime=0,
        high=2,
        low=3,
        open=1,
        close=4,
        volume=-1,
        openinterest=-1
        )
    
#        # Create a Data Feed
#        data1 = btfeeds.GenericCSVData(
#        dataname= datapath2,
#        fromdate=datetime.datetime(2017, 4, 1),
#        todate=datetime.datetime(2017, 12, 30),
#    
#        nullvalue=0,
#    
#        dtformat=('%d/%m/%Y %H:%M'),
#        timeframe=bt.TimeFrame.Minutes, 
#        compression=360,
#    
#    
#        date=-1,
#        datetime=0,
#        high=2,
#        low=3,
#        open=1,
#        close=4,
#        volume=-1,
#        openinterest=-1
#        )
#    
#        data1.plotinfo.plot = False
        # Add the Data Feed to Cerebro
        cerebro.adddata(data)
#        cerebro.adddata(data1)
        
       
        def printSQN(analyzer):
            sqn = round(analyzer.sqn,2)
            print('SQN: {}'.format(sqn))
    
        def printMDD(analyzer):
            mdd = round(analyzer.max.moneydown,2)
            print('MDD: {}'.format(mdd))
        
            
        # Set our desired cash start
        cerebro.broker.setcash(100000.0)     
    
        # Add a FixedSize sizer according to the stake
        cerebro.addsizer(bt.sizers.FixedSize, stake=1)
    
        # Set the commission - 0.1% ... divide by 100 to remove the %
        cerebro.broker.setcommission(commission=0.002)
    
        # Set the Slippage
    #    cerebro.broker.set_slippage_fixed(0)
    
      #  bt.observers.Trades.plotlines.pnlplus.color = 'blue'
     #   bt.observers.Trades.plotlines.pnlminus.marker = 'o'
        print('Results for Month - %d' % int(i))
        # Print out the starting conditions
        print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())
        
        # Analyzer
    
        cerebro.addanalyzer(bt.analyzers.SQN, _name="sqn")
        cerebro.addanalyzer(bt.analyzers.DrawDown, _name="drawdown")
        cerebro.addanalyzer(bt.analyzers.Transactions, _name="transactions")
        
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, _name="a_ol")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, filter='short',_name="s_ol")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, filter='long',_name="l_ol")
        
        # Run over everything
        strategies = cerebro.run()
        firstStrat = strategies[0]
        

        # Print out the final result
        print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())
        
        val += cerebro.broker.getvalue()
        t_el += firstStrat.elong_counter
        t_es += firstStrat.eshort_counter
        
        # print the analyzers 
        firstStrat.analyzers.a_ol.print()
#        firstStrat.analyzers.l_ol.print()
#        firstStrat.analyzers.s_ol.print()
        printSQN(firstStrat.analyzers.sqn.get_analysis())
        print('')

        printMDD(firstStrat.analyzers.drawdown.get_analysis())
    #    print(firstStrat.analyzers.transactions.get_analysis())
    
        print("El Stop Hits - {} | ES Stop Hits - {}" 
              .format(firstStrat.elsl_counter, firstStrat.essl_counter,
                      ))
#        print("SL - {}, Long TP - {}, Short TP - {}, MA1 - {}, MA2 - {}, Dev - {}" 
#          .format(firstStrat.slp1, firstStrat.ltp,
#                  firstStrat.stp, firstStrat.ma1,
#                  firstStrat.ma2, firstStrat.dev,))
        print('')
        print('ROE - %.2f | Net- %.2f | Total Longs - %d' % ((firstStrat.trad_roe -(firstStrat.elong_counter+firstStrat.eshort_counter)*0.4), val, t_el))
        lol += firstStrat.trad_roe
        print('SLP - %.2f | STP- %d | LTP - %d | Dev - %.2f' % (firstStrat.slp1, firstStrat.stp, firstStrat.ltp, firstStrat.dev))

    #    print('Total Avgs - %d, Long Avgs - %d' % ((firstStrat.long_avg_counter), firstStrat.long_avg_counter))
    #    firstStrat._trades[data][0].print()
#        print(round(val-100000*(i-3),2))
        print(' ')
        
        cerebro.plot()
print(round(val-900000,2))
print(lol)
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import datetime  # For datetime objects
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])
import warnings
warnings.filterwarnings("ignore")

# Import the backtrader platform
import backtrader as bt
import backtrader.feeds as btfeeds
from backtrader.indicators import hullstddev
from backtrader.analyzers import basictradestats

# Import Strategies
from BTCUSD_5L import BTCUSD_5L
from BTCUSD_5S import BTCUSD_5S
from BTCUSD_5SL import BTCUSD_5SL                         

lista = ['09','10','11','12','13','14','15','16','17']

#lista = ['11']
         
for i in lista :
    
    if __name__ == '__main__':
        # Create a cerebro entity
        cerebro = bt.Cerebro()
    
    #    strats = cerebro.optstrategy(
    #        BTCUSD_5,
    #        maperiod=range(10, 31))
        
        # Add a strategy
    #    cerebro.addstrategy(BTCUSD_5L)
    #    cerebro.addstrategy(BTCUSD_5S)
        cerebro.addstrategy(BTCUSD_5SL)
    
        # Datas are in a subfolder of the samples. Need to find where the script is
        # because it could have been called from anywhere
        modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
        datapath = os.path.join(modpath, 'data\XBatch\XAUUSD-'+i+'-5M.csv')
        datapath2 = os.path.join(modpath, 'data\XBatch\XAUUSD-'+i+'-6H.csv')
    
        # Create a Data Feed
        data = btfeeds.GenericCSVData(
        dataname= datapath,
        fromdate=datetime.datetime(int('20'+i), 1, 1),
        todate=datetime.datetime(int('20'+i), 12, 30),
    
        nullvalue=0,
    
        dtformat=('%d/%m/%Y %H:%M'),
        timeframe=bt.TimeFrame.Minutes, 
        compression=5,
    
    
        date=-1,
        datetime=0,
        high=2,
        low=3,
        open=1,
        close=4,
        volume=-1,
        openinterest=-1
        )
    
        # Create a Data Feed
        data1 = btfeeds.GenericCSVData(
        dataname= datapath2,
        fromdate=datetime.datetime(int('20'+i), 1, 1),
        todate=datetime.datetime(int('20'+i), 12, 30),
    
        nullvalue=0,
    
        dtformat=('%d/%m/%Y %H:%M'),
        timeframe=bt.TimeFrame.Minutes, 
        compression=360,
    
    
        date=-1,
        datetime=0,
        high=2,
        low=3,
        open=1,
        close=4,
        volume=-1,
        openinterest=-1
        )
    
    #    data1.plotinfo.plot = False
        # Add the Data Feed to Cerebro
        cerebro.adddata(data)
        cerebro.adddata(data1)
        
       
        def printSQN(analyzer):
            sqn = round(analyzer.sqn,2)
            print('SQN: {}'.format(sqn))
    
        def printMDD(analyzer):
            mdd = round(analyzer.max.moneydown,2)
            print('MDD: {}'.format(mdd))
        
    
        # Set our desired cash start
        cerebro.broker.setcash(100000.0)     
    
        # Add a FixedSize sizer according to the stake
        cerebro.addsizer(bt.sizers.FixedSize, stake=1)
    
        # Set the commission - 0.1% ... divide by 100 to remove the %
        cerebro.broker.setcommission(commission=0.00)
    
        # Set the Slippage
    #    cerebro.broker.set_slippage_fixed(0)
    
      #  bt.observers.Trades.plotlines.pnlplus.color = 'blue'
     #   bt.observers.Trades.plotlines.pnlminus.marker = 'o'
        print(' ')
        print('         For Year 20' + i)
     
        # Print out the starting conditions
        print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())
        
        # Analyzer
    
        cerebro.addanalyzer(bt.analyzers.SQN, _name="sqn")
        cerebro.addanalyzer(bt.analyzers.DrawDown, _name="drawdown")
        cerebro.addanalyzer(bt.analyzers.Transactions, _name="transactions")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, _name="lol")
        
        # Run over everything
        strategies = cerebro.run()
        firstStrat = strategies[0]
        
        # Print out the final result
        print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())
           
        # print the analyzers 
        firstStrat.analyzers.lol.print()
        printSQN(firstStrat.analyzers.sqn.get_analysis())
        printMDD(firstStrat.analyzers.drawdown.get_analysis())
    #    print(firstStrat.analyzers.transactions.get_analysis())
    
        print("El Stop Hits - {}, ES Stop Hits - {}, Stoch Hit {} - {} Times & Hit {} - {} Times" 
              .format(firstStrat.elsl_counter, firstStrat.essl_counter,
                      firstStrat.OBstoch_value, firstStrat.OB_stoch_count,
                      firstStrat.OSstoch_value, firstStrat.OS_stoch_count,))
    #    print('Total Avgs - %d, Long Avgs - %d' % ((firstStrat.long_avg_counter), firstStrat.long_avg_counter))
    #    print(firstStrat._trades[data][0])

        print(' ')
        cerebro.plot()
        print(' ')

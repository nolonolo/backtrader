from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import datetime  # For datetime objects
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])
import warnings
warnings.filterwarnings("ignore")

# Import the backtrader platform
import backtrader as bt
import backtrader.feeds as btfeeds
from backtrader.indicators import hullstddev
from backtrader.analyzers import basictradestats

# Import Strategies
#from BTCUSD_5L import BTCUSD_5L
#from BTCUSD_5S import BTCUSD_5S
#from BTCUSD_5SL import BTCUSD_5SL   

class BTCUSD_5SL(bt.Strategy):
    
    def log(self, txt, dt=None):
        #''' Logging function for this strategy'''
        dt = dt or self.datas[0].datetime.datetime(0)
        #print('%s' % (txt))

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.dataclose = self.datas[0].close
        

        # To keep track of pending orders and buy price/commission
        self.order = None
        self.buyprice = None
        self.buycomm = None

        # Indicators
        self.stoch = bt.indicators.StochasticFull(self.datas[0], period=14)
#        self.hull1 = bt.indicators.HullMovingAverage(self.datas[1], period=250)
        self.hull_d = hullstddev.Hullstddev(self.datas[0], period=250, period1 = 250, devfactor = 8)
        self.hull_d.plotinfo.plot = False
        self.ssma = bt.indicators.SmoothedMovingAverage(self.datas[1], period=4)
        self.ssma1 = bt.indicators.SmoothedMovingAverage(self.datas[1], period=3)
        
        # Variables
        self.slp1 = .3
        self.avg1 = 50
        self.stp = 0.4
        self.ltp = 0.4

        self.last_long_entry = 0
        self.last_short_entry = 0
        
        self.elong_counter = 0
        self.eshort_counter = 0
        
        self.long_avg_counter = 0
        
        self.elsl_counter = 0
        self.essl_counter = 0
        
        self.OB_stoch_count = 0
        self.OS_stoch_count = 0
        
        self.OBstoch_value = 97
        self.OSstoch_value = 3
        
        self.trad_roe = 0
        self.t_el = 0
        
        # Custom Position Counting
        self.elong_positions = 0
        self.eshort_positions = 0
        
    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enougth cash
        if order.status in [order.Completed]:
            if order.isbuy():
                self.log(
                    'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                    (order.executed.price,
                     order.executed.value,
                     order.executed.comm))

                self.buyprice = order.executed.price
                self.buycomm = order.executed.comm
                #self.log(self.position.size)
                

                
            else:  # Sell
                self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                         (order.executed.price,
                          order.executed.value,
                          order.executed.comm))
                #self.log(self.position.size)


            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        # Write down: no pending order
        self.order = None
        
    def notify_trade(self, trade):
        if not trade.isclosed:
            return

        self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f, POSITION OPEN %d' %
                 (trade.pnl, trade.pnlcomm,self.position.size))

    def next(self):
        
        # Simply log the closing price of the series from the reference
        #self.log("Close - %.2f | LE - %s | SE - %s " % (self.dataclose[0], self.last_long_entry, self.last_short_entry))
       # self.log('%.2f' % self.hull1.lines.hma[-1])
        # Check if an order is pending ... if yes, we cannot send a 2nd one
        if self.order:
            return
       
        #Trade Cond
        shull_tradable = self.ssma.lines.smma[0] > self.ssma1.lines.smma[0]
        lhull_tradable = self.ssma.lines.smma[0] < self.ssma1.lines.smma[0]
        
        # HullMA Trigger Calculation
#        lhull_tradable = True
#        shull_tradable = True
#        if self.hull1.lines.hma[0] > self.hull1.lines.hma[-5]:
#            hull_tradable = True
#        else:
#            hull_tradable = False
           
        # Avg Trigger Conditions
#        long_avg = self.elong_positions == 1 and self.dataclose[0] < self.last_long_entry*((100 - self.avg1)/100) 
#        short_avg = self.short_positions == -1 and self.dataclose[0] > self.last_short_entry*((100 + self.avg1)/100)
        
        # Stoploss Trigger Conditions
        long_sl = self.position.size > 0 and self.dataclose[0] < self.last_long_entry*((100 - self.slp1)/100)
        short_sl = self.position.size < 0 and self.dataclose[0] > self.last_short_entry*((100 + self.slp1)/100)

        # Stoploss Trigger Conditions
        short_tp = self.position.size < 0 and self.dataclose[0] < self.last_short_entry*((100 - self.stp)/100)
        long_tp = self.position.size > 0 and self.dataclose[0] > self.last_long_entry*((100 + self.ltp)/100)
               
        # Elong Trigger Conditions
        e_long = self.dataclose[0] < self.hull_d.lines.b_dev[0] and self.elong_positions == 0 and lhull_tradable 
        e_short = self.dataclose[0] > self.hull_d.lines.t_dev[0] and self.eshort_positions == 0 and shull_tradable 
        
        # Close Conditions
        e_long_close =  (self.stoch.lines.percDSlow[0] > self.OBstoch_value ) and self.elong_positions == 1 and (not long_tp )
        e_short_close =  (self.stoch.lines.percDSlow[0] < self.OSstoch_value ) and self.eshort_positions == -1 and (not short_tp )
        
        # Stoch counter
        if self.stoch.lines.percDSlow[0] > self.OBstoch_value:
            self.OB_stoch_count += 1
        elif self.stoch.lines.percDSlow[0] < self.OSstoch_value:
            self.OS_stoch_count += 1
        
        # Stoploss Trigger
        if long_sl:
            self.log('LONG STOP, %.2f' % self.dataclose[0])
            self.elong_positions = 0
            self.elsl_counter += 1
            self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100 
            self.last_long_entry = 0
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()
        
        elif short_sl:
            self.log('SHORT STOP, %.2f' % self.dataclose[0])
            self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100
            self.last_short_entry = 0
            self.essl_counter += 1
            self.eshort_positions = 0
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()     

        # Stoploss Trigger
        if long_tp:
            self.log('LONG TP, %.2f' % self.dataclose[0])
            self.elong_positions = 0
#            self.elsl_counter += 1
            self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100 
            self.last_long_entry = 200000
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()
        
        elif short_tp:
            self.log('SHORT TP, %.2f' % self.dataclose[0])
            self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100
            self.last_short_entry = 0
#            self.essl_counter += 1
            self.eshort_positions = 0
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()  
            
#        if long_avg:
#            self.log('E_LONG AVG, %.2f' % self.dataclose[0])
#            # Keep track of the created order to avoid a 2nd order
#            self.order = self.buy()
#            self.long_avg_counter += 1
#            self.elong_positions += 1
            
        
#        elif short_avg:
#            self.log('SHORT AVG, %.2f' % self.dataclose[0])
#            # Keep track of the created order to avoid a 2nd order
#            self.order = self.sell() 
#            self.short_avg_counter += 1
#            self.short_positions -= 1       
        
        # Check if we are in the market
        if self.position.size == 0:

            # Not yet ... we MIGHT BUY if ...
            if e_long:

                self.log('E_LONG, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                self.elong_counter += 1
                self.elong_positions = 1
                self.last_long_entry = self.dataclose[0]
                # Keep track of the created order to avoid a 2nd order
                self.order = self.buy()
            
            elif  e_short:
                
                self.log('E_SHORT, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                self.eshort_counter += 1
                self.eshort_positions = -1
                self.last_short_entry = self.dataclose[0]
                # Keep track of the created order to avoid a 2nd order
                self.order = self.sell()
                
                
        else:

            if  e_long_close:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('E_LONG CLOSE, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                self.trad_roe += -(self.last_long_entry - self.dataclose[0])/self.last_long_entry*100 
                self.elong_positions = 0
                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()
            
            elif  e_short_close:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('E_SHORT CLOSE, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                self.eshort_positions = 0
                self.trad_roe += (self.last_short_entry - self.dataclose[0])/self.last_short_entry*100
                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()
                      

lista = ['09','10','11','12','13','14','15','16','17']
#lista = ['17']
bista = lista
t_el = 0
         
for i in bista :
    
    if __name__ == '__main__':
        # Create a cerebro entity
        cerebro = bt.Cerebro()
    
    #    strats = cerebro.optstrategy(
    #        BTCUSD_5,
    #        maperiod=range(10, 31))
        
        # Add a strategy
#        cerebro.addstrategy(BTCUSD_5L)
    #    cerebro.addstrategy(BTCUSD_5S)
        cerebro.addstrategy(BTCUSD_5SL)
    
        # Datas are in a subfolder of the samples. Need to find where the script is
        # because it could have been called from anywhere
        modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
        datapath = os.path.join(modpath, 'data\XBatch\XAUUSD-'+i+'-5M.csv')
        datapath2 = os.path.join(modpath, 'data\XBatch\XAUUSD-'+i+'-6H.csv')
    
        # Create a Data Feed
        data = btfeeds.GenericCSVData(
        dataname= datapath,
        fromdate=datetime.datetime(int('20'+i), 1, 1),
        todate=datetime.datetime(int('20'+i), 12, 31),
    
        nullvalue=0,
    
        dtformat=('%d/%m/%Y %H:%M'),
        timeframe=bt.TimeFrame.Minutes, 
        compression=5,
    
    
        date=-1,
        datetime=0,
        high=2,
        low=3,
        open=1,
        close=4,
        volume=-1,
        openinterest=-1
        )
    
        # Create a Data Feed
        data1 = btfeeds.GenericCSVData(
        dataname= datapath2,
        fromdate=datetime.datetime(int('20'+i), 1, 1),
        todate=datetime.datetime(int('20'+i), 12, 31),
    
        nullvalue=0,
    
        dtformat=('%d/%m/%Y %H:%M'),
        timeframe=bt.TimeFrame.Minutes, 
        compression=360,
    
    
        date=-1,
        datetime=0,
        high=2,
        low=3,
        open=1,
        close=4,
        volume=-1,
        openinterest=-1
        )
    
    #    data1.plotinfo.plot = False
        # Add the Data Feed to Cerebro
        cerebro.adddata(data)
        cerebro.adddata(data1)
        
       
        def printSQN(analyzer):
            sqn = round(analyzer.sqn,2)
            print('SQN: {}'.format(sqn))
    
        def printMDD(analyzer):
            mdd = round(analyzer.max.moneydown,2)
            print('MDD: {}'.format(mdd))
        
    
        # Set our desired cash start
        cerebro.broker.setcash(10000.0)     
    
        # Add a FixedSize sizer according to the stake
        cerebro.addsizer(bt.sizers.FixedSize, stake=1)
    
        # Set the commission - 0.1% ... divide by 100 to remove the %
        cerebro.broker.setcommission(commission=0.00)
    
        # Set the Slippage
    #    cerebro.broker.set_slippage_fixed(0)
    
      #  bt.observers.Trades.plotlines.pnlplus.color = 'blue'
     #   bt.observers.Trades.plotlines.pnlminus.marker = 'o'
        print(' ')
        print('         For  /20'+i)
     
        # Print out the starting conditions
        print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())
        
        # Analyzer
    
        cerebro.addanalyzer(bt.analyzers.SQN, _name="sqn")
        cerebro.addanalyzer(bt.analyzers.DrawDown, _name="drawdown")
        cerebro.addanalyzer(bt.analyzers.Transactions, _name="transactions")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, _name="lol")
        
        # Run over everything
        strategies = cerebro.run()
        firstStrat = strategies[0]
        
        # Print out the final result
        print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())
          
        t_el += firstStrat.elong_counter
        # print the analyzers 
        firstStrat.analyzers.lol.print()
        printSQN(firstStrat.analyzers.sqn.get_analysis())
        printMDD(firstStrat.analyzers.drawdown.get_analysis())
    #    print(firstStrat.analyzers.transactions.get_analysis())
    
        print("El Stop Hits - {}, ES Stop Hits - {}, Stoch Hit {} - {} Times & Hit {} - {} Times" 
                  .format(firstStrat.elsl_counter, firstStrat.essl_counter,
                          firstStrat.OBstoch_value, firstStrat.OB_stoch_count,
                          firstStrat.OSstoch_value, firstStrat.OS_stoch_count,))
        print('Total Avgs - %d, Long Avgs - %d' % ((firstStrat.long_avg_counter), firstStrat.long_avg_counter))
#        print(firstStrat._trades[data][0])
        print('')
        print('ROE - %.2f | Total trades - %d' % (firstStrat.trad_roe, t_el))
#        lol += firstStrat.trad_roe
        print(' ')
        cerebro.plot()
#        print(' ')

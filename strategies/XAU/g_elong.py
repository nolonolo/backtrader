from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import datetime  # For datetime objects
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])

# Import the backtrader platform
import backtrader as bt
import backtrader.feeds as btfeeds
import backtrader.indicators as btind
from backtrader.indicators import hullrb
import backtrader.analyzers as btanalyzers
# Create a Stratey
class BTCUSD_5(bt.Strategy):
    
    def log(self, txt, dt=None):
        #''' Logging function for this strategy'''
        dt = dt or self.datas[0].datetime.datetime(0)
        #print('%s' % (txt))

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.dataclose = self.datas[0].close

        # To keep track of pending orders and buy price/commission
        self.order = None
        self.buyprice = None
        self.buycomm = None


        
        self.hull_en = hullrb.HullMovingRB(self.datas[0], period=250, perc = 1)
        self.stoch = bt.indicators.StochasticFull(self.datas[0], period=14)
        self.hull1 = bt.indicators.HullMovingAverage(self.datas[0], period=250)

        self.slp1 = 0.5
        self.avg1 = 50

        self.last_long_entry = 0
        self.elong_counter = 0
        self.long_avg_counter = 0
        self.sl_counter = 0
        self.stoch_count = 0
        self.stoch_value = 97

        # Custom Position Counting
        self.elong_positions = 0
        
    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enougth cash
        if order.status in [order.Completed]:
            if order.isbuy():
                self.log(
                    'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                    (order.executed.price,
                     order.executed.value,
                     order.executed.comm))

                self.buyprice = order.executed.price
                self.buycomm = order.executed.comm
                #self.log(self.position.size)
                

                
            else:  # Sell
                self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                         (order.executed.price,
                          order.executed.value,
                          order.executed.comm))
                #self.log(self.position.size)


            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        # Write down: no pending order
        self.order = None
        
    def notify_trade(self, trade):
        if not trade.isclosed:
            return

        self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f, POSITION OPEN %d' %
                 (trade.pnl, trade.pnlcomm,self.position.size))

    def next(self):
        
        # Simply log the closing price of the series from the reference
        #self.log("Close - %.2f | LE - %s | SE - %s " % (self.dataclose[0], self.last_long_entry, self.last_short_entry))
       # self.log('%.2f' % self.hull1.lines.hma[-1])
        # Check if an order is pending ... if yes, we cannot send a 2nd one
        if self.order:
            return
       
        # HullMA Trigger Calculation
        hull_tradable = True
        if self.hull1.lines.hma[0] > self.hull1.lines.hma[-10]:
            hull_tradable = True
        else:
            hull_tradable = False
           
        # Avg Trigger Conditions
        long_avg = self.elong_positions == 1 and self.dataclose[0] < self.last_long_entry*((100 - self.avg1)/100) 
#        short_avg = self.short_positions == -1 and self.dataclose[0] > self.last_short_entry*((100 + self.avg1)/100)
        
        # Stoploss Trigger Conditions
        long_sl = self.position.size > 0 and self.dataclose[0] < self.last_long_entry*((100 - self.slp1)/100)
#        short_sl = self.position.size < 0 and self.dataclose[0] > self.last_short_entry*((100 + self.slp1)/100)
               
        # Elong Trigger Conditions
        e_long = self.dataclose[0] < self.hull_en.lines.bot[0] and self.elong_positions == 0 
        e_long_close =  (self.stoch.lines.percDSlow[0] > self.stoch_value ) and self.elong_positions > 0 
        
        # Stoch counter
        if self.stoch.lines.percDSlow[0] > self.stoch_value:
            self.stoch_count += 1
        # Stoploss Trigger
        if long_sl:
            self.log('LONG STOP, %.2f' % self.dataclose[0])
            self.elong_positions = 0
            self.sl_counter += 1
            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()
        
#        elif short_sl:
#            self.log('SHORT STOP, %.2f' % self.dataclose[0])
#            self.last_short_entry = 0
            # Keep track of the created order to avoid a 2nd order
#            self.order = self.close()     

        if long_avg:
            self.log('E_LONG AVG, %.2f' % self.dataclose[0])
            # Keep track of the created order to avoid a 2nd order
            self.order = self.buy()
            self.long_avg_counter += 1
            self.elong_positions += 1
            
        
#        elif short_avg:
#            self.log('SHORT AVG, %.2f' % self.dataclose[0])
#            # Keep track of the created order to avoid a 2nd order
#            self.order = self.sell() 
#            self.short_avg_counter += 1
#            self.short_positions -= 1       
        
        # Check if we are in the market
        if self.position.size == 0:

            # Not yet ... we MIGHT BUY if ...
            if e_long:

                # BUY, BUY, BUY!!! (with all possible default parameters)
                self.log('E_LONG, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                self.elong_counter += 1
                self.e_long_on = True
                self.elong_positions = 1
                self.last_long_entry = self.dataclose[0]
                # Keep track of the created order to avoid a 2nd order
                self.order = self.buy()
                
        else:

            if  e_long_close:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('E_LONG CLOSE, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
                self.e_long_on = False
                self.elong_positions = 0
                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()
                


if __name__ == '__main__':
    # Create a cerebro entity
    cerebro = bt.Cerebro()

#    strats = cerebro.optstrategy(
#        BTCUSD_5,
#        maperiod=range(10, 31))
    
    # Add a strategy
    cerebro.addstrategy(BTCUSD_5)
    
    # Datas are in a subfolder of the samples. Need to find where the script is
    # because it could have been called from anywhere
    modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
    datapath = os.path.join(modpath, 'XAUUSD-16-5M.csv')

    # Create a Data Feed
    data = btfeeds.GenericCSVData(
    dataname= datapath,
    fromdate=datetime.datetime(2016, 2, 11),
    todate=datetime.datetime(2016, 2, 28),

    nullvalue=0,

    dtformat=('%d/%m/%Y %H:%M'),
    timeframe=bt.TimeFrame.Minutes, 
    compression=1,


    date=-1,
    datetime=0,
    high=2,
    low=3,
    open=1,
    close=4,
    volume=-1,
    openinterest=-1
    )
    
        
    # Add the Data Feed to Cerebro
    cerebro.adddata(data)
    
    def printTradeAnalysis(analyzer):

        #Get the results we are interested in
        total_open = analyzer.total.open
        total_closed = analyzer.total.closed
        total_won = analyzer.won.total
        total_lost = analyzer.lost.total
        pnl_avg = analyzer.pnl.net.average
        lose_streak = analyzer.streak.lost.longest
        pnl_net = round(analyzer.pnl.net.total,2)
        strike_rate = (total_won / total_closed) * 100
        pf = analyzer.pnl.gross.total/(analyzer.pnl.gross.total - pnl_net)
        avg_trade_time = analyzer.len.average * 5/60
        gt = round(analyzer.pnl.gross.total,2)
        
        #Designate the rows
        h1 = ['Total Open', 'Total Closed', 'Total Won', 'Total Lost', 'PF']
        h2 = ['Strike Rate','Avg Win', 'Losing Streak', 'PnL Net', 'Avg Time']
        r1 = [total_open, total_closed,total_won,total_lost, round(pf,2)]
        r2 = [round(strike_rate,2), round(pnl_avg,2), lose_streak, pnl_net,' %.2f Hrs '% (avg_trade_time)]
        #Check which set of headers is the longest.
        print(gt)
        
        if len(h1) > len(h2):
            header_length = len(h1)
        else:
            header_length = len(h2)
            #Print the rows
            print_list = [h1,r1,h2,r2]
            row_format ="{:<15}" * (header_length + 1)
            print("Trade Analysis Results:")
            for row in print_list:
                  print(row_format.format('',*row))
        

    
    def printSQN(analyzer):
        sqn = round(analyzer.sqn,2)
        print('SQN: {}'.format(sqn))

    def printMDD(analyzer):
        mdd = round(analyzer.max.moneydown,2)
        print('MDD: {}'.format(mdd))
    

    # Set our desired cash start
    cerebro.broker.setcash(100000.0)     

    # Add a FixedSize sizer according to the stake
    cerebro.addsizer(bt.sizers.FixedSize, stake=1)

    # Set the commission - 0.1% ... divide by 100 to remove the %
    cerebro.broker.setcommission(commission=0.00)

    # Set the Slippage
#    cerebro.broker.set_slippage_fixed(0)

    # Print out the starting conditions
    print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())
    
    # Analyzer
    cerebro.addanalyzer(bt.analyzers.TradeAnalyzer, _name="ta")
    cerebro.addanalyzer(bt.analyzers.SQN, _name="sqn")
    cerebro.addanalyzer(bt.analyzers.DrawDown, _name="drawdown")
    cerebro.addanalyzer(bt.analyzers.Transactions, _name="transactions")

    # Run over everything
    strategies = cerebro.run()
    firstStrat = strategies[0]
    
    # Print out the final result
    print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())
   
    
    # print the analyzers 
    printTradeAnalysis(firstStrat.analyzers.ta.get_analysis())
    printSQN(firstStrat.analyzers.sqn.get_analysis())
    printMDD(firstStrat.analyzers.drawdown.get_analysis())
#    print(firstStrat.analyzers.transactions.get_analysis())

    print("Elongs - {}, Stop Hits - {}, Stoch Hit {} - {} Times" .format(firstStrat.elong_counter, firstStrat.sl_counter, firstStrat.stoch_value, firstStrat.stoch_count))
    print('Total Avgs - %d, Long Avgs - %d' % ((firstStrat.long_avg_counter), firstStrat.long_avg_counter))
#    print(firstStrat._trades[data][0])


        
    cerebro.plot(style='candlestick', barup='green', bardown='red')

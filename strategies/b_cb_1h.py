from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import datetime  # For datetime objects
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])
import warnings
warnings.filterwarnings("ignore")

# Import the backtrader platform
import backtrader as bt
import backtrader.feeds as btfeeds
from backtrader.indicators import dmastddev

# Create a Stratey
class BTCUSD_5(bt.Strategy):
    

    
    def log(self, txt, dt=None):
        #''' Logging function for this strategy'''
        dt = dt or self.datas[0].datetime.datetime(0)
#        print('%s' % (txt))

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.dataclose = self.datas[0].close
        self.datahigh = self.datas[0].high
        self.datalow = self.datas[0].low

        # To keep track of pending orders and buy price/commission
        self.order = None
        self.buyprice = None
        self.buycomm = None
#
        self.sma = bt.indicators.SMA(self.dataclose,period=20)
        
        self.l.upbound = bt.indicators.basicops.Highest(self.datahigh,period=20,subplot=False)
        self.l.downbound = bt.indicators.basicops.Lowest(self.datalow,period=20,subplot=False)
#        
        # Variables
#        self.slp1 = 2.5
#        
#        self.stp = 10
#        self.ltp = 100
#        self.atr_trad = 10
        
        self.trad_roe2 = 0

        self.last_long_entry = 0
        self.last_short_entry = 0
        
        self.elong_counter = 0
        self.eshort_counter = 0
        
        self.elsl_counter = 0
        self.essl_counter = 0
        
        self.trad_roe = 0
        self.t_el = 0
        
        self.io = 0
        self.t_tl = 0
        self.t_c = 0
        
        # Custom Position Counting
        self.e_contracts = 0
        
    def start(self):

        self.mystats = open('mystats.csv', 'w')
        self.mystats.write('datetime,type,open,high,low,close\n')
        
    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enougth cash
        if order.status in [order.Completed]:
            lolol = 3
#            if order.isbuy():
#                self.log(
#                    'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
#                    (order.executed.price,
#                     order.executed.value,
#                     order.executed.comm))
#
#                self.buyprice = order.executed.price
#                self.buycomm = order.executed.comm
#                self.log(self.position.size)
#                
#            else:  # Sell
#                self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
#                         (order.executed.price,
#                          order.executed.value,
#                          order.executed.comm))
#                self.log(self.position.size)
#
#
#            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        # Write down: no pending order
        self.order = None
        
    def notify_trade(self, trade):
        if not trade.isclosed:
            return

        self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f, POSITION OPEN %d' %
                 (trade.pnl, trade.pnlcomm,self.position.size))
        self.Trad_roe = trade.pnlcomm/trade.price*100
        trad_len = trade.barlen
        self.log('ROE - %.2f ' % self.Trad_roe)
        self.log(' ')
        self.trad_roe2 += self.Trad_roe
        self.t_tl += trad_len
#        data_trades = self._trades[data][0]
#        print(data_trades[self.io])
#        self.io += 1
        
    def next_open(self):
        
        # Simply log the closing price of the series from the reference
        #self.log("Close - %.2f | LE - %s | SE - %s " % (self.dataclose[0], self.last_long_entry, self.last_short_entry))
       # self.log('%.2f' % self.hull1.lines.hma[-1])

        # Check if an order is pending ... if yes, we cannot send a 2nd one
#        if self.order:
#            return
        
        self.t_c += 1 # Candle Bar counter
        
        long = self.datahigh[0] > self.l.upbound[0]+.1
        short = self.datalow[0] < self.l.downbound[0]-.1
        
      
        # Elong Trigger Conditions
        e_long = long 
        e_short = short
       

        if self.e_contracts == 0 and self.sma[0] > 0:

            self.log('E_LONG, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
            self.elong_counter += 1
#            self.order = self.close()
            self.order = self.buy(size=1, exectype = bt.Order.Stop, price=self.l.upbound[1]+.1)
            self.e_contracts = 1

        if long and self.e_contracts == -1 :

            self.log('E_LONG, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
            self.elong_counter += 1
#            self.order = self.close()
            self.order = self.buy(size=2, exectype = bt.Order.Stop,price=self.l.upbound+0.1)
            self.e_contracts = 1
            
        if  short and self.e_contracts == +1 :
            
            self.log('E_SHORT, %.2f | %s' % (self.dataclose[0], self.datas[0].datetime.datetime(0)))
            self.eshort_counter += 1
#            self.order = self.close()
            self.order = self.sell(size=2, exectype = bt.Order.Stop,price=self.l.downbound-0.1)
            self.e_contracts = -1

            
#
            
    def stop(self):
        self.mystats.close()
#           
val = 0
lol = 0
t_el = 0
t_es = 0
#montha = [1,2,3,4,5,6,7,8,9,10,11,12]
montha = [7]
for i in montha:
        
    if __name__ == '__main__':
        # Create a cerebro entity
        cerebro = bt.Cerebro()
        cerebro.addobserver(bt.observers.BuySell)

        # Add a strategy
        cerebro.addstrategy(BTCUSD_5)
       
        # Datas are in a subfolder of the samples. Need to find where the script is
        # because it could have been called from anywhere
        modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
#        datapath = os.path.join(modpath, 'data\IOTAUSDT-5M.csv')
        datapath = os.path.join(modpath, 'data\BTCUSD-All-1H.csv')
    
        # Create a Data Feed
        data = btfeeds.GenericCSVData(
        dataname= datapath,
#        fromdate=datetime.datetime(2017, 4, 1),
#        todate=datetime.datetime(2018, 5, 10),
        fromdate=datetime.datetime(2013, 1, 1),
        todate=datetime.datetime(2018, 12, 31),    
#        nullvalue=0,
    
        dtformat=('%d/%m/%Y %H:%M'),
        timeframe=bt.TimeFrame.Minutes, 
        compression=60,
    
    
        date=-1,
        datetime=0,
        high=2,
        low=3,
        open=1,
        close=4,
        volume=-1,
        openinterest=-1
        )
    
        # Add the Data Feed to Cerebro
        cerebro.adddata(data)
#        cerebro.adddata(data1)
        
       
        def printSQN(analyzer):
            sqn = round(analyzer.sqn,2)
            print('SQN: {}'.format(sqn))
    
        def printMDD(analyzer):
            mdd_p = round(analyzer.max.drawdown,2)
            mdd_n = round(analyzer.max.moneydown,2)
            print('MDD: {} % - {} $ '.format(mdd_p,mdd_n ))
        
            
        # Set our desired cash start
        cerebro.broker.setcash(100000.0)     
    
        # Set the commission - 0.1% ... divide by 100 to remove the %
        cerebro.broker.setcommission(commission=0.001)

        name = os.path.splitext(os.path.basename(__file__))[0]
        
        writeFile = open("Results/"+str(name)+".csv", "w")
        cerebro.addwriter(bt.WriterFile, out = writeFile, csv=True, rounding=2)
        
        print('Results for Month - %d' % i)
        # Print out the starting conditions
        print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())
        
        # Analyzer
    
        cerebro.addanalyzer(bt.analyzers.SQN, _name="sqn")
        cerebro.addanalyzer(bt.analyzers.DrawDown, _name="drawdown")
        cerebro.addanalyzer(bt.analyzers.Transactions, _name="transactions")
        
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, _name="a_ol")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, filter='short',_name="s_ol")
        cerebro.addanalyzer(bt.analyzers.basictradestats.BasicTradeStats, filter='long',_name="l_ol")
        
        # Run over everything
        strategies = cerebro.run(cheat_on_open=True)
        firstStrat = strategies[0]
        

        # Print out the final result
        print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())
        
        val += cerebro.broker.getvalue()
        t_el += firstStrat.elong_counter
        t_es += firstStrat.eshort_counter
        
        # print the analyzers 
        firstStrat.analyzers.a_ol.print()
        firstStrat.analyzers.l_ol.print()
        firstStrat.analyzers.s_ol.print()
        printSQN(firstStrat.analyzers.sqn.get_analysis())
        print('')

        printMDD(firstStrat.analyzers.drawdown.get_analysis())
    
        print("El Stop Hits - {} | ES Stop Hits - {}" 
              .format(firstStrat.elsl_counter, firstStrat.essl_counter,
                      ))

        print('')
        print('ROE - %.2f | Net- %.2f | Total Longs - %d' % (firstStrat.trad_roe2, (cerebro.broker.getvalue() - 100000), t_el))
        print("Total Bars - %d, Trade Bars - %d, Time Open  - %s" % ((firstStrat.t_c), firstStrat.t_tl, (str(int(firstStrat.t_tl/firstStrat.t_c*100)) + '%') ))

        print(' ')
        
        cerebro.plot(style='candle',volume=False)
        writeFile.close()
       

import pandas as pd
import numpy as np

df = pd.read_csv( 'Results/'+str(name)+".csv", skiprows=2, parse_dates = [0] , dayfirst=True, 
                     usecols=[ 3 ,16 , 26])

df.columns = ['Date','value','ROE']

df = df.dropna(subset=['value'])

pd.to_datetime(df['Date'], format='%Y/%m/%d %H:%M')

df.index = pd.to_datetime(df['Date'])
df.drop(['Date'], axis = 1)
#ohlc_dict = {'value':'last', 'ROE':'sum'}
#
#df = df.resample('7D', how=ohlc_dict)
cols=['value', 'ROE']  
df = df[cols]
#df = df.T
df.to_csv('Results/BTC-CB-W.csv')
import pandas as pd
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])
import numpy as np

modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
#datapath = os.path.join(modpath, 'E://Anaconda3/Lib/site-packages/backtrader/strategies/data/BTCUSD-18-1M-V.csv')
datapath = os.path.join(modpath, 'data/AAPL/AAPL-17-1M-2.csv')


df = pd.read_csv(datapath,
            parse_dates={'DateTime': ['Date', 'Time']},dayfirst = True,
            usecols=['Date', 'Time', 'open', 'high', 'low', 'close' , 'volume'],
            na_values=[np.nan]).set_index('DateTime')

#df = pd.read_csv(datapath,
#            parse_dates=['timestamp'],dayfirst = True,
#            usecols=['timestamp', 'open', 'high', 'low', 'close' , 'volume'],
#            na_values=[np.nan]).set_index('timestamp')

# Created a dictionary to tell Pandas how to re-sample, if this isn't in place it will re-sample each column separately 

ohlc_dict = {'open':'first', 'high':'max', 'low':'min', 'close': 'last', 'volume': 'sum'}

# Resample to 5Min (this format is needed) as per ohlc_dict, then remove any line with a NaN
df = df.resample('1Min', how=ohlc_dict)



# Resample mixes the columns so lets re-arrange them 
cols=['open', 'high', 'low', 'close', 'volume']  
df = df[cols]
#   df.replace('0', np.nan)

df = df[np.isfinite(df['open'])]

# Write out to CSV
df.to_csv("data/AAPL-17-1M-2.csv")

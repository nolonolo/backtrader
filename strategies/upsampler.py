import pandas as pd
import os.path  # To manage paths
import sys  # To find out the script name (in argv[0])
import numpy as np
import os

modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
#datapath = os.path.join(modpath, 'E://Anaconda3/Lib/site-packages/backtrader/strategies/data/BTCUSD-18-1M-V.csv')
datapath = os.path.join(modpath, 'C:/Users/sumit93/Anaconda3/Lib/site-packages/qbroker/REST/IOTAUSDT-5M.csv')
datapath1 = os.path.join(modpath, 'C:/Users/sumit93/Anaconda3/Lib/site-packages/qbroker/REST/IOTAUSDT-5Ms.csv')

df = pd.read_csv(datapath,
            usecols=['datetime', 'open', 'high', 'low', 'close' , 'volume'],
            na_values=[np.nan])

slicer = lambda x: x[:16]
df1 = df[df.columns[0]].apply(slicer)
df2 = df.drop('datetime', 1)
df3 = pd.concat([df1, df2], axis=1)
df3.set_index('datetime', inplace=True)
df3.index = pd.to_datetime(df3.index)
df4 = df3.sort_index()  
with open(datapath1, 'a') as f:
         (df4).to_csv(f, header=True)

dfn = pd.read_csv(datapath1,
            usecols=['datetime', 'open', 'high', 'low', 'close' , 'volume'],
            na_values=[np.nan])
slicer = lambda x: x[:16]
dfn1 = dfn[dfn.columns[0]].apply(slicer)
dfn2 = dfn.drop('datetime', 1)
dfn3 = pd.concat([dfn1, dfn2], axis=1)
print(dfn3)  
# opening the file with w+ mode truncates the file
f = open(datapath1, "w+")
f.close()
dfn3.set_index('datetime', inplace=True)
with open(datapath1, 'a') as f:
         (dfn3).to_csv(f, header=True)       

